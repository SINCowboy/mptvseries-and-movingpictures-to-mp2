# MPTVSeries and MovingPictures to MP2

**MP-TVSeries & Moving Pictures metadata and pictures to Media Portal 2**

Version 1.0
Software and source shared as GNU GPLv3

Change log: 
- 1.0 First released version.

Credits: 
- raffe (original creator) 

I have tried to follow the picture and nfo guidelines here:
* https://www.team-mediaportal.com/wiki/display/MediaPortal2/NfoMovieMetadataExtractor
* https://www.team-mediaportal.com/wiki/display/glossary/NFO+Node+-+Lexical+support
* https://kodi.wiki/view/TV-Show_artwork
* https://kodi.wiki/view/NFO_files/TV_shows


For TV series it works best if you have:
1. One folder for each TV series, eg. maybe "The.Blacklist"
1. In the TV series folder you have one folder for each seson, eg. maybe "S01"


I also recommend using Trakt.

You may find some more info about this tool here: https://forum.team-mediaportal.com/threads/export-tool-and-import-support-from-mp-tvseries-moving-pictures-to-mp2.138887/

Credits: 

*  raffe (original creator) 

This tool will not alter any media files. It will make .nfo files and copy 
pictures from Moving Pictures and MPTVSeries to their media folders. 

These .nfos and pictures may make it easier to move from MP1 to MP2. 

You need to have this software on a computer that can find both 
the MP1 files and the media folders. You also need to have sqlite3_x64.dll
in the same folder as the application.

If, or probably when, you get problems. Check the content of the 
database, maybe with the tool here: 
https://sqlitebrowser.org/ 

Some examples: Is the filename to long in database (more than 260 char)? 
Do the online_series have empty rows? Etc... 

Also check the folder, do the files have the right name, season & 
episode etc? Check with www.thetvdb.com.  

If nothing helps, maybe you need to install autoit and make your 
own error checks in the code. You may find some Media Portal help in this forum:  
https://forum.team-mediaportal.com/categories/mediaportal-2.528/ 

You should find the source here:  
https://gitlab.com/raffe1234/mptvseries-and-movingpictures-to-mp2/tree/master 

And some Autoit help in this forum: 
https://www.autoitscript.com/forum/forum/43-autoit-help-and-support 