#include <ButtonConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <ListViewConstants.au3>
#include <GuiListView.au3>
#include <SQLite.au3>
#include <SQLite.dll.au3>
#include <FileConstants.au3>
#include <StaticConstants.au3>
#include <Timers.au3>

; Set a HotKey
HotKeySet("x", "CancelProgress")
HotKeySet("X", "CancelProgress")
HotKeySet("{F1}", "ShowHelp")

Global $sAbout = "MovingPictures & MP-TVSeries metadata and pictures to MP2" & @CRLF & "- Version 1.0" & @CRLF
$sAbout = $sAbout & "- Software and source shared as GNU GPLv3. " & @CRLF & @CRLF
$sAbout = $sAbout & "Change log: " & @CRLF & @CRLF
$sAbout = $sAbout & " - 1.0 First released version." & @CRLF & @CRLF
$sAbout = $sAbout & "Credits: " & @CRLF
$sAbout = $sAbout & "raffe (original creator) " & @CRLF

Global $sHelp = "This tool will not alter any media files. It will make .nfo files and copy "
$sHelp = $sHelp & "pictures from Moving Pictures and MPTVSeries to their media folders. "
$sHelp = $sHelp & "These .nfos and pictures may make it easier to move from MP1 to MP2. " & @CRLF & @CRLF

$sHelp = $sHelp & "You need to have this software on a computer that can find both "
$sHelp = $sHelp & "the MP1 files and the media folders. You also need to have sqlite3_x64.dll "
$sHelp = $sHelp & "in the same folder as the application." & @CRLF & @CRLF

$sHelp = $sHelp & "If, or probably when, you get problems. Check the content of the "
$sHelp = $sHelp & "database, maybe with the tool here: " & @CRLF
$sHelp = $sHelp & "https://sqlitebrowser.org/ " & @CRLF & @CRLF

$sHelp = $sHelp & "Some examples: Is the filename to long in database (more than 260 char)? "
$sHelp = $sHelp & "Do the online_series have empty rows? Etc... " & @CRLF & @CRLF

$sHelp = $sHelp & "Also check the folder, do the files have the right name, season & " & @CRLF
$sHelp = $sHelp & "episode etc? Check with www.thetvdb.com.  " & @CRLF & @CRLF

$sHelp = $sHelp & "If nothing helps, maybe you need to install autoit and make your "

$sHelp = $sHelp & "own error checks in the code. You may find some Media Portal help in this forum:  " & @CRLF
$sHelp = $sHelp & "https://forum.team-mediaportal.com/categories/mediaportal-2.528/ " & @CRLF & @CRLF

$sHelp = $sHelp & "You should find the source here:  " & @CRLF
$sHelp = $sHelp & "https://gitlab.com/raffe1234/mptvseries-and-movingpictures-to-mp2/tree/master " & @CRLF & @CRLF

$sHelp = $sHelp & "And some Autoit help in this forum: " & @CRLF
$sHelp = $sHelp & "https://www.autoitscript.com/forum/forum/43-autoit-help-and-support "

Global $sSQLiteDll_Filename = @ScriptDir & "\sqlite3_x64.dll"
Global $sThisNewDatabaseFileHasBeenChosen, $sThisNewProgramDataFolderHasBeenChosen
Global $iReturnValue, $iReturnValueMemory
Global $iCancel = 0
Global $sec, $min, $hr, $day, $TimerSet = 0, $TimeStart, $TimeDiff, $TimeGone, $TimeCalculatedStillLeft
Global $sErrorLog, $iErrorLogCounter = 0

Global $sTVSeriesDatabase = "C:\ProgramData\Team MediaPortal\MediaPortal\database\TVSeriesDatabase4.db3"
Global $sTVSeriesProgramDataFolder = "C:\ProgramData\Team MediaPortal\MediaPortal"
Global $aTVSeriesStatusUpdateInfo[6]
Global $aTVSeriesSelectedPathIDs[0]
Global $aTVSeriesPaths, $iTVSeriesPathRows, $iTVSeriesPathColumns
Global $aTVSeriesLocalEpisodes, $iTVSeriesLocalEpisodesRows, $iTVSeriesLocalEpisodesColumns
Global $aTVSeriesLocalSeries, $iTVSeriesLocalSeriesRows, $iTVSeriesLocalSeriesColumns

Global $sMovingPicturesDatabase = "C:\ProgramData\Team MediaPortal\MediaPortal\database\movingpictures.db3"
Global $aMovingPicturesStatusUpdateInfo[6]
Global $aMovingPicturesSelectedPathIDs[0], $aMovingPicturesLocalMediaInfoMovies[0][2]
Global $aMovingPicturesPaths, $iMovingPicturesPathRows, $iMovingPicturesPathColumns
Global $aMovingPicturesLocalMedia, $iMovingPicturesLocalMediaRows, $iMovingPicturesLocalMediaColumns

#Region ### START Koda GUI section ### Form=
Global $Form1_1 = GUICreate("MovingPictures & MP-TVSeries metadata to MP2", 1006, 666, -1, -1, $WS_OVERLAPPEDWINDOW)
Global $MenuFile = GUICtrlCreateMenu("&File")
Global $MenuExit = GUICtrlCreateMenuItem("&Exit", $MenuFile)
Global $MenuHelp = GUICtrlCreateMenu("&Help")
Global $MenuHelpInfo = GUICtrlCreateMenuItem("&Help" & @TAB & "F1", $MenuHelp)
Global $MenuAbout = GUICtrlCreateMenuItem("&About", $MenuHelp)
Global $Form1_AccelTable[1][2] = [["{F1}", $MenuHelp]]
Global $LabelMainForm = GUICtrlCreateLabel("This tool will not alter any media file. It will make .nfo files and copy pictures from Moving Pictures and MPTVSeries to their media folders. These .nfos/pictures may make it easier to move from MP1 to MP2.", 10, 10, 970, 20)
Global $tabMain = GUICtrlCreateTab(8, 32, 985, 585)
;Stuff for Moving Pictures tab
;****************************
Global $TabSheet1 = GUICtrlCreateTabItem("Moving Pictures")
Global $GroupMovingPictures1 = GUICtrlCreateGroup("Step 1: Chose database", 12, 62, 393, 113)
Global $LabelMovingPicturesDatabaseFile = GUICtrlCreateLabel("Chosen database: " & $sMovingPicturesDatabase, 24, 81, 368, 50)
Global $ButtonMovingPicturesChangeDatabase = GUICtrlCreateButton("Change Database", 20, 140, 99, 25)
GUICtrlCreateGroup("", -99, -99, 1, 1)
Global $GroupMovingPictures2 = GUICtrlCreateGroup("Step 2: Chose folders", 12, 182, 393, 209)
Global $ListViewMovingPicturesPath = GUICtrlCreateListView("|ID|Path", 16, 203, 378, 150, -1, BitOR($WS_EX_CLIENTEDGE, $LVS_EX_CHECKBOXES))
GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 0, 25)
GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 1, 25)
GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 2, 300)
;GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 0, 25)
;GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 1, 25)
;GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 2, 300)
Global $ButtonMovingPicturesPathCheckAll = GUICtrlCreateButton("Check All", 100, 358, 75, 25)
Global $ButtonMovingPicturesPathUncheckall = GUICtrlCreateButton("Uncheck all", 181, 358, 75, 25)
Global $ButtonMovingPicturesPathGetFolders = GUICtrlCreateButton("Get folders", 20, 358, 75, 25)
Global $ButtonMovingPicturesFolderSelectionInfo = GUICtrlCreateButton("Folder selection info", 261, 358, 115, 25)
GUICtrlCreateGroup("", -99, -99, 1, 1)
Global $GroupMovingPictures3 = GUICtrlCreateGroup("Step 3: Chose metadata", 12, 398, 393, 210)
Global $CheckboxMovingPicturesCreateNfo = GUICtrlCreateCheckbox("Create .nfo file based on the information in the database", 24, 419, 313, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
Global $CheckboxMovingPicturesCopyFanart = GUICtrlCreateCheckbox("Copy Fanart picture from database location to file location", 24, 443, 313, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
Global $CheckboxMovingPicturesCopyCover = GUICtrlCreateCheckbox("Copy Cover/Poster picture from database location to file location", 24, 467, 323, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
Global $CheckboxMovingPicturesCopyThumb = GUICtrlCreateCheckbox("Copy Thumb picture from database location to file location", 24, 491, 313, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
Global $CheckboxMovingPicturesSameTimestampAsMedia = GUICtrlCreateCheckbox("Same Timestamp on new files as on media file", 24, 515, 313, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
Global $CheckboxMovingPicturesReplaceExistingFiles = GUICtrlCreateCheckbox("Replace existing files", 24, 539, 313, 17)
Global $GroupMovingPictures4 = GUICtrlCreateGroup("Step 4: Execute", 412, 62, 521, 329)
Global $RadioMovingPicturesCreateMetadata = GUICtrlCreateRadio("CREATE selected metadata in selected folders", 424, 83, 239, 17)
Global $RadioMovingPicturesDeleteMetadata = GUICtrlCreateRadio("DELETE selected metadata in selected folders", 680, 83, 239, 17)
Global $ButtonMovingPicturesCreateMetadata = GUICtrlCreateButton("Create Metadata", 426, 107, 99, 25)
Global $ButtonMovingPicturesDeleteMetadata = GUICtrlCreateButton("Delete Metadata", 682, 107, 99, 25)
GUICtrlSetState(-1, $GUI_DISABLE)
Global $LabelMovingPicturesWorking = GUICtrlCreateLabel("Please wait, working (press x to cancel)...", 464, 168, 395, 59)
GUICtrlSetFont(-1, 15, 400, 0, "Arial")
GUICtrlSetState($LabelMovingPicturesWorking, $GUI_HIDE)
Global $LabelMovingPictures = GUICtrlCreateLabel("** Moving Pictures **", 412, 30, 395, 59)
GUICtrlSetFont(-1, 15, 400, 0, "Arial")
Global $ProgressMovingPictures = GUICtrlCreateProgress(430, 265, 486, 17)
Global $GroupMovingPicturesStatus = GUICtrlCreateGroup("Status", 416, 291, 505, 89)
GUICtrlCreateGroup("", -99, -99, 1, 1)
Global $LabelMovingPicturesStatus = GUICtrlCreateLabel(" ", 423, 307, 500, 74, $SS_LEFT)
GUICtrlCreateGroup("", -99, -99, 1, 1)
;Stuff for TV Series tab
;****************************
Global $TabSheet2 = GUICtrlCreateTabItem("MP-TVSeries")
Global $GroupTVSeries1 = GUICtrlCreateGroup("Step 1: Chose database and ProgramData folder", 12, 62, 393, 113)
Global $LabelTVSeriesDatabaseFile = GUICtrlCreateLabel("Chosen database: " & $sTVSeriesDatabase, 24, 81, 368, 50)
Global $LabelTVSeriesProgramDataFolder = GUICtrlCreateLabel("Chosen ProgramData folder: " & $sTVSeriesProgramDataFolder, 24, 111, 368, 35)
Global $ButtonTVSeriesChangeDatabase = GUICtrlCreateButton("Change Database", 20, 140, 99, 25)
Global $ButtonTVSeriesChangeProgramDataFolder = GUICtrlCreateButton("Change ProgramData Folder", 140, 140, 150, 25)
GUICtrlCreateGroup("", -99, -99, 1, 1)
Global $GroupTVSeries2 = GUICtrlCreateGroup("Step 2: Chose folders", 12, 182, 393, 209)
Global $ListViewTVSeriesPath = GUICtrlCreateListView("|ID|Enabled|Path", 16, 203, 378, 150, -1, BitOR($WS_EX_CLIENTEDGE, $LVS_EX_CHECKBOXES))
GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 0, 25)
GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 1, 25)
GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 2, 25)
GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 3, 300)
;GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 0, 25)
;GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 1, 25)
;GUICtrlSendMsg(-1, $LVM_SETCOLUMNWIDTH, 2, 300)
Global $ButtonTVSeriesPathCheckAll = GUICtrlCreateButton("Check All", 100, 358, 75, 25)
Global $ButtonTVSeriesPathUncheckall = GUICtrlCreateButton("Uncheck all", 181, 358, 75, 25)
Global $ButtonTVSeriesPathGetFolders = GUICtrlCreateButton("Get folders", 20, 358, 75, 25)
Global $ButtonTVSeriesFolderSelectionInfo = GUICtrlCreateButton("Folder selection info", 261, 358, 115, 25)
GUICtrlCreateGroup("", -99, -99, 1, 1)
Global $GroupTVSeries3 = GUICtrlCreateGroup("Step 3: Chose metadata", 12, 398, 393, 210)
Global $CheckboxTVSeriesCreateSeriesNfo = GUICtrlCreateCheckbox("Create SERIES .nfo file based on the information in the database", 24, 419, 323, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
Global $CheckboxTVSeriesCopySeriesFanart = GUICtrlCreateCheckbox("Copy SERIES Fanart pictures", 24, 443, 170, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
Global $CheckboxTVSeriesCopySeriesSeasonPoster = GUICtrlCreateCheckbox("Copy SERIES Season Poster pics", 200, 443, 180, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
Global $CheckboxTVSeriesCopySeriesBanner = GUICtrlCreateCheckbox("Copy SERIES Banner picture", 24, 467, 170, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
Global $CheckboxTVSeriesCopySeriesPoster = GUICtrlCreateCheckbox("Copy SERIES Poster picture", 200, 467, 170, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
Global $CheckboxTVSeriesCreateEpisodesNfo = GUICtrlCreateCheckbox("Create EPISODES .nfo files based on the information in the database", 24, 491, 340, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
Global $CheckboxTVSeriesCopyEpisodePics = GUICtrlCreateCheckbox("Copy EPISODE pictures", 24, 515, 340, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
Global $CheckboxTVSeriesSameTimestampAsMedia = GUICtrlCreateCheckbox("Same Timestamp on new EPISODE files as on media file", 24, 539, 313, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
Global $CheckboxTVSeriesReplaceExistingFiles = GUICtrlCreateCheckbox("Replace existing files", 24, 563, 313, 17)
Global $GroupTVSeries4 = GUICtrlCreateGroup("Step 4: Execute", 412, 62, 521, 329)
Global $RadioTVSeriesCreateMetadata = GUICtrlCreateRadio("CREATE selected metadata in selected folders", 424, 83, 239, 17)
Global $RadioTVSeriesDeleteMetadata = GUICtrlCreateRadio("DELETE selected metadata in selected folders", 680, 83, 239, 17)
Global $ButtonTVSeriesCreateMetadata = GUICtrlCreateButton("Create Metadata", 426, 107, 99, 25)
Global $ButtonTVSeriesDeleteMetadata = GUICtrlCreateButton("Delete Metadata", 682, 107, 99, 25)
GUICtrlSetState(-1, $GUI_DISABLE)
Global $LabelTVSeriesWorking = GUICtrlCreateLabel("Please wait, working (press x to cancel)...", 434, 168, 475, 118, $SS_CENTER)
GUICtrlSetFont(-1, 15, 400, 0, "Arial")
GUICtrlSetState($LabelTVSeriesWorking, $GUI_HIDE)
Global $LabelTVSeries = GUICtrlCreateLabel("** MP-TVSeries **", 412, 30, 395, 59)
GUICtrlSetFont(-1, 15, 400, 0, "Arial")
Global $LabelTVSeriesLogSeriesHeader = GUICtrlCreateLabel("Series log (last 3 rows)", 415, 398, 395, 17)
GUICtrlSetFont(-1, 8, 800, 0, "MS Sans Serif")
Global $LabelTVSeriesLogSeriesRow0 = GUICtrlCreateLabel("...", 415, 422, 570, 25)
Global $LabelTVSeriesLogSeriesRow1 = GUICtrlCreateLabel("...", 415, 446, 570, 25)
Global $LabelTVSeriesLogSeriesRow2 = GUICtrlCreateLabel("...", 415, 470, 570, 25)
Global $LabelTVSeriesLogEpisodesHeader = GUICtrlCreateLabel("Episodes log (last 4 rows)", 415, 494, 395, 17)
GUICtrlSetFont(-1, 8, 800, 0, "MS Sans Serif")
Global $LabelTVSeriesLogEpisodesRow0 = GUICtrlCreateLabel("...", 415, 518, 570, 25)
Global $LabelTVSeriesLogEpisodesRow1 = GUICtrlCreateLabel("...", 415, 542, 570, 25)
Global $LabelTVSeriesLogEpisodesRow2 = GUICtrlCreateLabel("...", 415, 566, 570, 25)
Global $LabelTVSeriesLogEpisodesRow3 = GUICtrlCreateLabel("...", 415, 590, 570, 25)
Global $ProgressTVSeries = GUICtrlCreateProgress(430, 265, 486, 17)
Global $GroupTVSeriesStatus = GUICtrlCreateGroup("Status", 416, 291, 505, 89)
GUICtrlCreateGroup("", -99, -99, 1, 1)
Global $LabelTVSeriesStatus = GUICtrlCreateLabel(" ", 423, 307, 500, 74, $SS_LEFT)
GUICtrlCreateGroup("", -99, -99, 1, 1)
GUICtrlCreateTabItem("")
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

TVSeriesStatusUpdateNewRow("Ready")
MovingPicturesStatusUpdateNewRow("Ready")
StartMainGUI()

Func ShowHelp()
	MsgBox(0, "Help", $sHelp)
EndFunc   ;==>ShowHelp

Func CancelProgress()
	; The HotKey was pressed, so set the flag
	$iCancel = 1
EndFunc   ;==>CancelProgress

Func TimeNow($ProcentLeft)

	Local $TimeLeftClac

	If $TimerSet = 0 Then
		$TimeStart = _Timer_Init()
		$TimerSet = 1
	EndIf

	$TimeDiff = _Timer_Diff($TimeStart) / 1000

	$sec = Mod($TimeDiff, 60)
	$min = Mod($TimeDiff / 60, 60)
	$hr = Floor($TimeDiff / 60 ^ 2)
	$day = $hr / 24
	$hr = Mod($hr, 24)

	$TimeGone = StringFormat("%02i days %02i:%02i:%02i", $day, $hr, $min, $sec)

	$TimeLeftClac = $TimeDiff * (100 / $ProcentLeft) - $TimeDiff
	If $TimeLeftClac < 0 Then
		$TimeLeftClac = 0
	EndIf

	$sec = Mod($TimeLeftClac, 60)
	$min = Mod($TimeLeftClac / 60, 60)
	$hr = Floor($TimeLeftClac / 60 ^ 2)
	$day = $hr / 24
	$hr = Mod($hr, 24)

	$TimeCalculatedStillLeft = StringFormat("%02i days %02i:%02i:%02i", $day, $hr, $min, $sec)

EndFunc   ;==>TimeNow

Func TimeLeft()


EndFunc   ;==>TimeLeft

Func TVSeriesStatusUpdateNewRow($sTextToAddToStatus)

	For $x = 0 To 3
		$aTVSeriesStatusUpdateInfo[$x] = $aTVSeriesStatusUpdateInfo[$x + 1]
	Next
	$aTVSeriesStatusUpdateInfo[4] = $sTextToAddToStatus

	$aTVSeriesStatusUpdateInfo[5] = ""
	For $x = 0 To 4
		$aTVSeriesStatusUpdateInfo[5] = $aTVSeriesStatusUpdateInfo[5] & $aTVSeriesStatusUpdateInfo[$x] & @CRLF
	Next

	GUICtrlSetData($LabelTVSeriesStatus, $aTVSeriesStatusUpdateInfo[5])

EndFunc   ;==>TVSeriesStatusUpdateNewRow

Func MovingPicturesStatusUpdateNewRow($sTextToAddToStatus)

	For $x = 0 To 3
		$aMovingPicturesStatusUpdateInfo[$x] = $aMovingPicturesStatusUpdateInfo[$x + 1]
	Next
	$aMovingPicturesStatusUpdateInfo[4] = $sTextToAddToStatus

	$aMovingPicturesStatusUpdateInfo[5] = ""
	For $x = 0 To 4
		$aMovingPicturesStatusUpdateInfo[5] = $aMovingPicturesStatusUpdateInfo[5] & $aMovingPicturesStatusUpdateInfo[$x] & @CRLF
	Next

	GUICtrlSetData($LabelMovingPicturesStatus, $aMovingPicturesStatusUpdateInfo[5])

EndFunc   ;==>MovingPicturesStatusUpdateNewRow

Func TVSeriesStatusUpdateMoreOnLastRow($sTextToAddToStatusLastRow)

	$aTVSeriesStatusUpdateInfo[4] = $aTVSeriesStatusUpdateInfo[4] & " " & $sTextToAddToStatusLastRow

	$aTVSeriesStatusUpdateInfo[5] = ""
	For $x = 0 To 4
		$aTVSeriesStatusUpdateInfo[5] = $aTVSeriesStatusUpdateInfo[5] & $aTVSeriesStatusUpdateInfo[$x] & @CRLF
	Next

	GUICtrlSetData($LabelTVSeriesStatus, $aTVSeriesStatusUpdateInfo[5])

EndFunc   ;==>TVSeriesStatusUpdateMoreOnLastRow

Func MovingPicturesStatusUpdateMoreOnLastRow($sTextToAddToStatusLastRow)

	$aMovingPicturesStatusUpdateInfo[4] = $aMovingPicturesStatusUpdateInfo[4] & " " & $sTextToAddToStatusLastRow

	$aMovingPicturesStatusUpdateInfo[5] = ""
	For $x = 0 To 4
		$aMovingPicturesStatusUpdateInfo[5] = $aMovingPicturesStatusUpdateInfo[5] & $aMovingPicturesStatusUpdateInfo[$x] & @CRLF
	Next

	GUICtrlSetData($LabelMovingPicturesStatus, $aMovingPicturesStatusUpdateInfo[5])

EndFunc   ;==>MovingPicturesStatusUpdateMoreOnLastRow

Func TVSeriesStatusUpdateReplaceLastRow($sTextToReplaceLastRow)

	$aTVSeriesStatusUpdateInfo[4] = $sTextToReplaceLastRow

	$aTVSeriesStatusUpdateInfo[5] = ""
	For $x = 0 To 4
		$aTVSeriesStatusUpdateInfo[5] = $aTVSeriesStatusUpdateInfo[5] & $aTVSeriesStatusUpdateInfo[$x] & @CRLF
	Next

	GUICtrlSetData($LabelTVSeriesStatus, $aTVSeriesStatusUpdateInfo[5])

EndFunc   ;==>TVSeriesStatusUpdateReplaceLastRow

Func MovingPicturesStatusUpdateReplaceLastRow($sTextToReplaceLastRow)

	$aMovingPicturesStatusUpdateInfo[4] = $sTextToReplaceLastRow

	$aMovingPicturesStatusUpdateInfo[5] = ""
	For $x = 0 To 4
		$aMovingPicturesStatusUpdateInfo[5] = $aMovingPicturesStatusUpdateInfo[5] & $aMovingPicturesStatusUpdateInfo[$x] & @CRLF
	Next

	GUICtrlSetData($LabelMovingPicturesStatus, $aMovingPicturesStatusUpdateInfo[5])

EndFunc   ;==>MovingPicturesStatusUpdateReplaceLastRow

Func SelectProgramDataFolder()
	; Create a constant variable in Local scope of the message to display in FileOpenDialog.
	Local Const $sMessage = "Select a folder."

	; Display an open dialog to select a file.
	Local $sFolderSelectDialog = FileSelectFolder($sMessage, "")
	If @error Then
		; Display the error message.
		;MsgBox($MB_SYSTEMMODAL, "", "No file was selected.")
		; Change the working directory (@WorkingDir) back to the location of the script directory as FileOpenDialog sets it to the last accessed folder.
		FileChangeDir(@ScriptDir)
		$sThisNewProgramDataFolderHasBeenChosen = "None"
	Else
		; Change the working directory (@WorkingDir) back to the location of the script directory as FileOpenDialog sets it to the last accessed folder.
		FileChangeDir(@ScriptDir)

		; Display the selected file.
		$sThisNewProgramDataFolderHasBeenChosen = $sFolderSelectDialog
	EndIf
EndFunc   ;==>SelectProgramDataFolder

Func SelectDatabaseFile()
	; Create a constant variable in Local scope of the message to display in FileOpenDialog.
	Local Const $sMessage = "Select a database."

	; Display an open dialog to select a file.
	Local $sFileOpenDialog = FileOpenDialog($sMessage, "C:\", "Database (*.db3)", $FD_FILEMUSTEXIST)
	If @error Then
		; Display the error message.
		;MsgBox($MB_SYSTEMMODAL, "", "No file was selected.")
		; Change the working directory (@WorkingDir) back to the location of the script directory as FileOpenDialog sets it to the last accessed folder.
		FileChangeDir(@ScriptDir)
		$sThisNewDatabaseFileHasBeenChosen = "None"
	Else
		; Change the working directory (@WorkingDir) back to the location of the script directory as FileOpenDialog sets it to the last accessed folder.
		FileChangeDir(@ScriptDir)

		; Display the selected file.
		$sThisNewDatabaseFileHasBeenChosen = $sFileOpenDialog
	EndIf
EndFunc   ;==>SelectDatabaseFile

Func MovingPicturesGetFolders()

	MovingPicturesStatusUpdateNewRow("Starting SQLite... ")
	_SQLite_Startup($sSQLiteDll_Filename, False, 1)
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "SQLite Error", "SQLite3.dll Can't be Loaded!" & $sSQLiteDll_Filename)
		Exit -1
	EndIf
;~ 	ConsoleWrite("_SQLite_LibVersion=" & _SQLite_LibVersion() & @CRLF) ; For testing in console

	MovingPicturesStatusUpdateMoreOnLastRow("Opening database... ")
	Local $hDskDb = _SQLite_Open($sMovingPicturesDatabase) ; Open a permanent disk database
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "SQLite Error", "Can't open or create a permanent Database!")
		Exit -1
	EndIf

	MovingPicturesStatusUpdateMoreOnLastRow("Getting folders... ")
	Local $aResulOfSqlQuery, $iReturnValue
	$iReturnValue = _SQLite_GetTable2d(-1, "SELECT * FROM import_path;", $aResulOfSqlQuery, $iMovingPicturesPathRows, $iMovingPicturesPathColumns)
	If $iReturnValue = $SQLITE_OK Then
		_GUICtrlListView_DeleteAllItems($ListViewMovingPicturesPath)
		Global $aMovingPicturesPaths[$iMovingPicturesPathRows][3]
		For $a = 0 To $iMovingPicturesPathRows - 1
			For $b = 0 To 1 ; Will only show ID and paths for every row. 0 = ID, 1 = Path
				$aMovingPicturesPaths[$a][$b + 1] = $aResulOfSqlQuery[$a + 1][$b]
			Next
			MovingPicturesStatusUpdateMoreOnLastRow($a + 1 & ". ")
			;			ConsoleWrite($aMovingPicturesPaths[$a][1] & @CRLF) ; For testing in console
		Next
		_GUICtrlListView_AddArray($ListViewMovingPicturesPath, $aMovingPicturesPaths)
;~ 		_SQLite_Display2DResult($aResulOfSqlQuery) ; For testing in console
	Else
		MsgBox($MB_SYSTEMMODAL, "SQLite Error: " & $iReturnValue, _SQLite_ErrMsg())
	EndIf

	_SQLite_Close($hDskDb) ; DB is a regular file that could be reopened later
	_SQLite_Shutdown()

EndFunc   ;==>MovingPicturesGetFolders

Func TVSeriesGetFolders()

	TVSeriesStatusUpdateNewRow("Starting SQLite... ")

	_SQLite_Startup($sSQLiteDll_Filename, False, 1)
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "SQLite Error", "SQLite3.dll Can't be Loaded!" & $sSQLiteDll_Filename)
		Exit -1
	EndIf
;~ 	ConsoleWrite("_SQLite_LibVersion=" & _SQLite_LibVersion() & @CRLF) ; For testing in console

	TVSeriesStatusUpdateMoreOnLastRow("Opening database... ")
	Local $hDskDb = _SQLite_Open($sTVSeriesDatabase) ; Open a permanent disk database
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "SQLite Error", "Can't open or create a permanent Database!")
		Exit -1
	EndIf

	TVSeriesStatusUpdateMoreOnLastRow("Getting folders... ")
	Local $aResulOfSqlQuery, $iReturnValue
	$iReturnValue = _SQLite_GetTable2d(-1, "SELECT * FROM ImportPathes;", $aResulOfSqlQuery, $iTVSeriesPathRows, $iTVSeriesPathColumns)
	If $iReturnValue = $SQLITE_OK Then
		_GUICtrlListView_DeleteAllItems($ListViewTVSeriesPath)
		Global $aTVSeriesPaths[$iTVSeriesPathRows][4]
		For $a = 0 To $iTVSeriesPathRows - 1
			For $b = 0 To 2 ; Will only show ID and paths for every row. 0 = ID, 2 = Path
				$aTVSeriesPaths[$a][$b + 1] = $aResulOfSqlQuery[$a + 1][$b]
			Next
			TVSeriesStatusUpdateMoreOnLastRow($a + 1 & ". ")
			;ConsoleWrite("Info: " & $aTVSeriesPaths[$a][0] & " " & $aTVSeriesPaths[$a][2] & @CRLF) ; For testing in console
		Next
		_GUICtrlListView_AddArray($ListViewTVSeriesPath, $aTVSeriesPaths)
		;_SQLite_Display2DResult($aResulOfSqlQuery)  ; For testing in console
		;_SQLite_Display2DResult($aTVSeriesPaths) ; For testing in console
	Else
		MsgBox($MB_SYSTEMMODAL, "SQLite Error: " & $iReturnValue, _SQLite_ErrMsg())
	EndIf

	_SQLite_Close($hDskDb) ; DB is a regular file that could be reopened later
	_SQLite_Shutdown()

EndFunc   ;==>TVSeriesGetFolders

Func MovingPicturesGetCheckedFolderCheckboxes()

	Local $iMovingPicturesSelectedPathsCount = 0

	For $x = 1 To $iMovingPicturesPathRows
		If _GUICtrlListView_GetItemChecked($ListViewMovingPicturesPath, $x - 1) Then
			$iMovingPicturesSelectedPathsCount += 1
		EndIf
	Next

	Global $aMovingPicturesSelectedPathIDs[$iMovingPicturesSelectedPathsCount]
	Global $aMovingPicturesSelectedPaths[$iMovingPicturesSelectedPathsCount]

	$iMovingPicturesSelectedPathsCount = 0
	For $y = 1 To $iMovingPicturesPathRows
		If _GUICtrlListView_GetItemChecked($ListViewMovingPicturesPath, $y - 1) Then
			$aMovingPicturesSelectedPathIDs[$iMovingPicturesSelectedPathsCount] = _GUICtrlListView_GetItemText($ListViewMovingPicturesPath, $y - 1, 1)
			$aMovingPicturesSelectedPaths[$iMovingPicturesSelectedPathsCount] = _GUICtrlListView_GetItemText($ListViewMovingPicturesPath, $y - 1, 2)
			$iMovingPicturesSelectedPathsCount += 1
		EndIf
	Next

	MovingPicturesStatusUpdateNewRow("Folder selection: Starting SQLite... ")
	_SQLite_Startup($sSQLiteDll_Filename, False, 1)
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "SQLite Error", "SQLite3.dll Can't be Loaded!" & $sSQLiteDll_Filename)
		Exit -1
	EndIf
;~ 	ConsoleWrite("_SQLite_LibVersion=" & _SQLite_LibVersion() & @CRLF) ; For testing in console

	MovingPicturesStatusUpdateMoreOnLastRow("Opening database... ")
	Local $hDskDb = _SQLite_Open($sMovingPicturesDatabase) ; Open a permanent disk database
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "SQLite Error", "Can't open or create a permanent Database!")
		Exit -1
	EndIf

	MovingPicturesStatusUpdateNewRow("Sum of movies: ")
	Global $aMovingPicturesLocalMediaInfoMovies[UBound($aMovingPicturesSelectedPathIDs)][2], $iMovingPicturesLocalMediaTotalNumberOfMovies = 0
	For $x = 1 To UBound($aMovingPicturesSelectedPathIDs)
		$iReturnValue = _SQLite_GetTable2d(-1, "SELECT * FROM local_media WHERE importpath = " & $aMovingPicturesSelectedPathIDs[$x - 1] & ";", $aMovingPicturesLocalMedia, $iMovingPicturesLocalMediaRows, $iMovingPicturesLocalMediaColumns)
		If $iReturnValue = $SQLITE_OK Then
			$aMovingPicturesLocalMediaInfoMovies[$x - 1][0] = "ID: " & $aMovingPicturesSelectedPathIDs[$x - 1]
			$aMovingPicturesLocalMediaInfoMovies[$x - 1][1] = UBound($aMovingPicturesLocalMedia) - 1
			$iMovingPicturesLocalMediaTotalNumberOfMovies += UBound($aMovingPicturesLocalMedia) - 1
			;ConsoleWrite(" Now tot. movies = " & $iMovingPicturesLocalMediaTotalNumberOfMovies)
			;_ArrayDisplay($aMovingPicturesLocalMedia,"ID: " & $aMovingPicturesSelectedPathIDs[$x - 1])
			MovingPicturesStatusUpdateMoreOnLastRow(" > " & $iMovingPicturesLocalMediaTotalNumberOfMovies)
		Else
			MsgBox($MB_SYSTEMMODAL, "SQLite Error: " & $iReturnValue, _SQLite_ErrMsg())
		EndIf
	Next

	; Remove all info in array to save memory
	$aMovingPicturesLocalMedia = 0

	_SQLite_Close($hDskDb) ; DB is a regular file that could be reopened later
	_SQLite_Shutdown()

EndFunc   ;==>MovingPicturesGetCheckedFolderCheckboxes

Func TVSeriesGetCheckedFolderCheckboxes()

	Local $iTVSeriesSelectedPathsCount = 0

	For $x = 1 To $iTVSeriesPathRows
		If _GUICtrlListView_GetItemChecked($ListViewTVSeriesPath, $x - 1) Then
			$iTVSeriesSelectedPathsCount += 1
		EndIf
	Next

	Global $aTVSeriesSelectedPathIDs[$iTVSeriesSelectedPathsCount]
	Global $aTVSeriesSelectedPaths[$iTVSeriesSelectedPathsCount]

	$iTVSeriesSelectedPathsCount = 0
	For $y = 1 To $iTVSeriesPathRows
		If _GUICtrlListView_GetItemChecked($ListViewTVSeriesPath, $y - 1) Then
			$aTVSeriesSelectedPathIDs[$iTVSeriesSelectedPathsCount] = _GUICtrlListView_GetItemText($ListViewTVSeriesPath, $y - 1, 1)
			$aTVSeriesSelectedPaths[$iTVSeriesSelectedPathsCount] = _GUICtrlListView_GetItemText($ListViewTVSeriesPath, $y - 1, 3)
			;ConsoleWrite(" Path: " & $aTVSeriesSelectedPaths[$iTVSeriesSelectedPathsCount] )
			$iTVSeriesSelectedPathsCount += 1

		EndIf
	Next

	TVSeriesStatusUpdateNewRow("Folder selection: Starting SQLite... ")
	_SQLite_Startup($sSQLiteDll_Filename, False, 1)
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "SQLite Error", "SQLite3.dll Can't be Loaded!" & $sSQLiteDll_Filename)
		Exit -1
	EndIf
;~ 	ConsoleWrite("_SQLite_LibVersion=" & _SQLite_LibVersion() & @CRLF) ; For testing in console

	TVSeriesStatusUpdateMoreOnLastRow("Opening database... ")
	Local $hDskDb = _SQLite_Open($sTVSeriesDatabase) ; Open a permanent disk database
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "SQLite Error", "Can't open or create a permanent Database!")
		Exit -1
	EndIf

	TVSeriesStatusUpdateNewRow("Total sum of selected series/episodes: ")
	;ConsoleWrite("Ubound = " & UBound($aTVSeriesSelectedPaths)  & @CRLF)
	;_ArrayDisplay($aTVSeriesSelectedPaths)
	Global $iTVSeriesLocalEpisodesTotalNumberOfEpisodes = 0
	Global $iTVSeriesLocalEpisodesTotalNumberOfSeries = 0
	Global $aTVSeriesLocalSeriesAndEpisodes[UBound($aTVSeriesSelectedPaths)][3]
	Global $aTVSeriesSelectedEpisodeCount[UBound($aTVSeriesSelectedPaths)]
	Global $aTVSeriesSelectedSeriesCount[UBound($aTVSeriesSelectedPaths)]
	For $x = 1 To UBound($aTVSeriesSelectedPaths)
		$iReturnValue = _SQLite_QuerySingleRow(-1, "SELECT COUNT(DISTINCT SeriesID) FROM local_episodes WHERE VolumeLabel = """ & $aTVSeriesSelectedPaths[$x - 1] & """;", $aTVSeriesSelectedSeriesCount)
		If $iReturnValue = $SQLITE_OK Then
			$aTVSeriesLocalSeriesAndEpisodes[$x - 1][0] = "Path: " & $aTVSeriesSelectedPaths[$x - 1]
			$aTVSeriesLocalSeriesAndEpisodes[$x - 1][1] = $aTVSeriesSelectedSeriesCount[0]
			$iTVSeriesLocalEpisodesTotalNumberOfSeries += $aTVSeriesSelectedSeriesCount[0]
			;ConsoleWrite(" Now tot. series = " & $iTVSeriesLocalEpisodesTotalNumberOfSeries)
			;_ArrayDisplay($aMovingPicturesLocalMedia,"ID: " & $aMovingPicturesSelectedPathIDs[$x - 1])
			TVSeriesStatusUpdateReplaceLastRow("Total sum of selected series/episodes: " & $iTVSeriesLocalEpisodesTotalNumberOfSeries)
		Else
			MsgBox($MB_SYSTEMMODAL, "SQLite Error: " & $iReturnValue, _SQLite_ErrMsg())
		EndIf
		;ConsoleWrite("Search = " & "SELECT count(*) FROM local_episodes WHERE VolumeLabel = """ & $aTVSeriesSelectedPaths[$x - 1] & """;" & @CRLF)
		$iReturnValue = _SQLite_QuerySingleRow(-1, "SELECT count(*) FROM local_episodes WHERE VolumeLabel = """ & $aTVSeriesSelectedPaths[$x - 1] & """;", $aTVSeriesSelectedEpisodeCount)
		If $iReturnValue = $SQLITE_OK Then
			$aTVSeriesLocalSeriesAndEpisodes[$x - 1][2] = $aTVSeriesSelectedEpisodeCount[0]
			$iTVSeriesLocalEpisodesTotalNumberOfEpisodes += $aTVSeriesSelectedEpisodeCount[0]
			;ConsoleWrite(" Now tot. episodes = " & $iTVSeriesLocalEpisodesTotalNumberOfEpisodes)
			;_ArrayDisplay($aMovingPicturesLocalMedia,"ID: " & $aMovingPicturesSelectedPathIDs[$x - 1])
			TVSeriesStatusUpdateMoreOnLastRow(" / " & $iTVSeriesLocalEpisodesTotalNumberOfEpisodes)
		Else
			MsgBox($MB_SYSTEMMODAL, "SQLite Error: " & $iReturnValue, _SQLite_ErrMsg())
		EndIf
	Next

	_SQLite_Close($hDskDb) ; DB is a regular file that could be reopened later
	_SQLite_Shutdown()

EndFunc   ;==>TVSeriesGetCheckedFolderCheckboxes

Func TVSeriesFolderSelectionInfo()

	;Get checked folders
	TVSeriesGetCheckedFolderCheckboxes()
	;Show info
	_ArrayDisplay($aTVSeriesLocalSeriesAndEpisodes, "Info about selected paths", Default, Default, Default, "Path|Series count|Episodes count")

EndFunc   ;==>TVSeriesFolderSelectionInfo

Func MovingPicturesFolderSelectionInfo()

	;Get checked folders
	MovingPicturesGetCheckedFolderCheckboxes()
	;Show info
	_ArrayDisplay($aMovingPicturesLocalMediaInfoMovies, "Info about selected paths", Default, Default, Default, "Path ID|Movies count")

EndFunc   ;==>MovingPicturesFolderSelectionInfo

Func TVSeriesDeleteFiles()

	GUICtrlSetData($LabelTVSeriesWorking, "Please wait, deleting files (press x to cancel)...")
	GUICtrlSetState($LabelTVSeriesWorking, $GUI_SHOW)

	;Disable Delete create button while we do this, so the user don't get any ideas ;-)...
	GUICtrlSetState($RadioTVSeriesCreateMetadata, $GUI_DISABLE)

	;Disable Create button to avoid dubble clicking...
	GUICtrlSetState($ButtonTVSeriesDeleteMetadata, $GUI_DISABLE)

	;Set progress = 0
	GUICtrlSetData($ProgressTVSeries, 0)

	;Get checked folders
	TVSeriesGetCheckedFolderCheckboxes()
	;$aMovingPicturesLocalMediaInfoMovies have all movies from checked folders

	TimeNow(1)

	;First we get the data we need from the database into arrays, so it will go faster when searching info

	If $iCancel = 1 Then
		; YES $IDYES (6)
		; NO $IDNO (7)
		TVSeriesStatusUpdateNewRow("Do you want to cancel?")
		$CancelMessageBox = MsgBox(4, "Cancel?", "Do you want to cancel?")
		;ConsoleWrite("box = " & $CancelMessageBox)
		If $CancelMessageBox = 6 Then

			TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = Yes.")
			$aMovingPicturesLocalMedia = 0

			;Enable Delete radio button again...
			GUICtrlSetState($RadioTVSeriesDeleteMetadata, $GUI_ENABLE)

			GUICtrlSetState($RadioTVSeriesCreateMetadata, $GUI_ENABLE)
			GUICtrlSetState($LabelTVSeriesWorking, $GUI_HIDE)
			GUICtrlSetState($TabSheet1, $GUI_ENABLE)

			$iCancel = 0
			$TimerSet = 0
			;ExitLoop
			Return

		Else
			TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = No.")
			$iCancel = 0
		EndIf
	EndIf

	TVSeriesStatusUpdateNewRow("Starting deleting... ")
	TVSeriesStatusUpdateNewRow("...")
	;Run this once for every checked folder
	;First we get the common folder for each series
	Local $aFoldersWithFilesToDelete, $sGetError, $sWholePath
	For $iCounterSelectedSeriesPaths = 1 To UBound($aTVSeriesSelectedPaths)

		;ConsoleWrite($aTVSeriesSelectedPaths[$iCounterSelectedSeriesPaths - 1] & @CRLF)
		$aFoldersWithFilesToDelete = _FileListToArrayRec($aTVSeriesSelectedPaths[$iCounterSelectedSeriesPaths - 1] & "\", "*", $FLTAR_FOLDERS, $FLTAR_RECUR, $FLTAR_NOSORT, $FLTAR_RELPATH)
		For $FileToDelete = 1 To $aFoldersWithFilesToDelete[0]

			If $iCancel = 1 Then
				; YES $IDYES (6)
				; NO $IDNO (7)
				TVSeriesStatusUpdateNewRow("Do you want to cancel?")
				$CancelMessageBox = MsgBox(4, "Cancel?", "Do you want to cancel?")
				;ConsoleWrite("box = " & $CancelMessageBox)
				If $CancelMessageBox = 6 Then

					TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = Yes.")
					$aMovingPicturesLocalMedia = 0

					;Enable Delete radio button again...
					GUICtrlSetState($RadioTVSeriesDeleteMetadata, $GUI_ENABLE)

					GUICtrlSetState($RadioTVSeriesCreateMetadata, $GUI_ENABLE)
					GUICtrlSetState($LabelTVSeriesWorking, $GUI_HIDE)
					GUICtrlSetState($TabSheet1, $GUI_ENABLE)

					$iCancel = 0
					$TimerSet = 0
					;ExitLoop
					Return

				Else
					TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = No.")
					$iCancel = 0
				EndIf
			EndIf

			$sWholePath = $aTVSeriesSelectedPaths[$iCounterSelectedSeriesPaths - 1] & "\" & $aFoldersWithFilesToDelete[$FileToDelete]

			If GUICtrlRead($CheckboxTVSeriesCreateSeriesNfo) = 1 Then
				TVSeriesStatusUpdateMoreOnLastRow(" nfo ")
				$sGetError = FileDelete($sWholePath & "tvshow.nfo")
				;ConsoleWrite($sGetError & " Delete: " & $sWholePath & "tvshow.nfo" & @CRLF)
			EndIf

			If GUICtrlRead($CheckboxTVSeriesCopySeriesFanart) = 1 Then
				TVSeriesStatusUpdateMoreOnLastRow(" fanart")
				$sGetError = FileDelete($sWholePath & "fanart*.jpg")
				;ConsoleWrite($sGetError & " Delete: " & $sWholePath & "fanart*.jpg" & @CRLF)
			EndIf

			If GUICtrlRead($CheckboxTVSeriesCopySeriesSeasonPoster) = 1 Then
				TVSeriesStatusUpdateMoreOnLastRow(" season")
				$sGetError = FileDelete($sWholePath & "season??-poster.jpg")
				;ConsoleWrite($sGetError & " Delete: " & $sWholePath & "season??-poster.jpg" & @CRLF)
			EndIf

			If GUICtrlRead($CheckboxTVSeriesCopySeriesBanner) = 1 Then
				TVSeriesStatusUpdateMoreOnLastRow(" banner")
				$sGetError = FileDelete($sWholePath & "banner.jpg")
				;ConsoleWrite($sGetError & " Delete: " & $sWholePath & "banner.jpg" & @CRLF)
			EndIf

			If GUICtrlRead($CheckboxTVSeriesCopySeriesPoster) = 1 Then
				TVSeriesStatusUpdateMoreOnLastRow(" poster")
				$sGetError = FileDelete($sWholePath & "poster.jpg")
				;ConsoleWrite($sGetError & " Delete: " & $sWholePath & "poster.jpg" & @CRLF)
			EndIf

			If GUICtrlRead($CheckboxTVSeriesCreateEpisodesNfo) = 1 Then
				TVSeriesStatusUpdateMoreOnLastRow(" all nfo")
				$sGetError = FileDelete($sWholePath & "*.nfo")
				;ConsoleWrite($sGetError & " Delete: " & $sWholePath & "*.nfo" & @CRLF)
			EndIf

			If GUICtrlRead($CheckboxTVSeriesCopyEpisodePics) = 1 Then
				TVSeriesStatusUpdateMoreOnLastRow(" all pics")
				$sGetError = FileDelete($sWholePath & "*.jpg")
				;ConsoleWrite($sGetError & " Delete: " & $sWholePath & "*.jpg" & @CRLF)
			EndIf

			Local $sNewStatusInfo = "On folder: " & $iCounterSelectedSeriesPaths * $FileToDelete & "/"
			$sNewStatusInfo = $sNewStatusInfo & UBound($aTVSeriesSelectedPaths) * $aFoldersWithFilesToDelete[0]
			TVSeriesStatusUpdateReplaceLastRow($sNewStatusInfo)
			GUICtrlSetData($ProgressTVSeries, 100 * ($iCounterSelectedSeriesPaths * $FileToDelete) / (UBound($aTVSeriesSelectedPaths) * $aFoldersWithFilesToDelete[0]))
		Next

	Next

	TVSeriesStatusUpdateNewRow("Done deleting!")

	GUICtrlSetState($RadioTVSeriesCreateMetadata, $GUI_ENABLE)

	GUICtrlSetState($ButtonTVSeriesDeleteMetadata, $GUI_ENABLE)

	GUICtrlSetState($LabelTVSeriesWorking, $GUI_HIDE)
EndFunc   ;==>TVSeriesDeleteFiles


Func TVSeriesCreateXMLandCopyPictures()

	;Disable Delete radio button while we do this, so the user don't get any ideas ;-)...
	GUICtrlSetState($RadioTVSeriesDeleteMetadata, $GUI_DISABLE)

	;Disable Create button to avoid dubble clicking...
	GUICtrlSetState($ButtonTVSeriesCreateMetadata, $GUI_DISABLE)

	;Set progress = 0
	GUICtrlSetData($ProgressTVSeries, 0)

	;Get checked folders
	TVSeriesGetCheckedFolderCheckboxes()
	;$aMovingPicturesLocalMediaInfoMovies have all movies from checked folders

	Local $iTotalDoneSeries = 0
	Local $iTotalDoneEpisodes = 0

	TimeNow(100 * ($iTotalDoneSeries / ((UBound($aTVSeriesLocalSeries) - 2) * UBound($aTVSeriesSelectedPaths))))

	Local $iReplaceFiles = 0
	If GUICtrlRead($CheckboxTVSeriesReplaceExistingFiles) = 1 Then
		$iReplaceFiles = 1
	EndIf

	TVSeriesStatusUpdateNewRow("Starting SQLite... ")
	_SQLite_Startup($sSQLiteDll_Filename, False, 1)
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "SQLite Error", "SQLite3.dll Can't be Loaded!" & $sSQLiteDll_Filename)
		Exit -1
	EndIf
;~ 	ConsoleWrite("_SQLite_LibVersion=" & _SQLite_LibVersion() & @CRLF) ; For testing in console

	TVSeriesStatusUpdateMoreOnLastRow("Opening database... ")
	Local $hDskDb = _SQLite_Open($sTVSeriesDatabase) ; Open a permanent disk database
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "SQLite Error", "Can't open or create a permanent Database!")
		Exit -1
	EndIf

	GUICtrlSetData($LabelTVSeriesWorking, "Please wait, step 1/3 preparing (press x to cancel)..." & @CRLF & "Wait ~ 1 min, when db in RAM = All Faster!")
	;First we get the data we need from the database into arrays, so it will go faster when searching info

	Local $iReturnValueMemoryOnline_series, $iReturnValueMemoryActors, $iReturnValueMemorySeason, $iReturnValueMemoryFanart
	GUICtrlSetData($ProgressTVSeries, 3)

	Local $aTVSeriesAllActors, $aTVSeriesAllActorsRows, $aTVSeriesAllActorsColumns
	Local $aTVSeriesFindAllActorsForOneSeries, $aTVSeriesGetAllActorsForOneSeries
	TVSeriesStatusUpdateNewRow("Wait 1/6... Getting all actors from database to array (this may take some time)...")
	$iReturnValueMemoryActors = _SQLite_GetTable2d(-1, "SELECT * FROM Actors ORDER BY SeriesID;", $aTVSeriesAllActors, $aTVSeriesAllActorsRows, $aTVSeriesAllActorsColumns)
	GUICtrlSetData($ProgressTVSeries, 100 * (1 / 6))

	If $iCancel = 1 Then
		; YES $IDYES (6)
		; NO $IDNO (7)
		TVSeriesStatusUpdateNewRow("Do you want to cancel?")
		$CancelMessageBox = MsgBox(4, "Cancel?", "Do you want to cancel?")
		;ConsoleWrite("box = " & $CancelMessageBox)
		If $CancelMessageBox = 6 Then
			_SQLite_Close($hDskDb)                     ; DB is a regular file that could be reopened later
			_SQLite_Shutdown()

			TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = Yes.")
			$aMovingPicturesLocalMedia = 0

			;Enable Delete radio button again...
			GUICtrlSetState($RadioTVSeriesDeleteMetadata, $GUI_ENABLE)

			GUICtrlSetState($RadioTVSeriesCreateMetadata, $GUI_ENABLE)
			GUICtrlSetState($LabelTVSeriesWorking, $GUI_HIDE)
			GUICtrlSetState($TabSheet1, $GUI_ENABLE)

			$aTVSeriesAllActors = 0

			$iCancel = 0
			$TimerSet = 0
			;ExitLoop
			Return

		Else
			TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = No.")
			$iCancel = 0
		EndIf
	EndIf

	Local $aTVSeriesAllLocalEpisodes, $aTVSeriesAllLocalEpisodesRows, $aTVSeriesAllLocalEpisodesColumns
	Local $aTVSeriesFindSeriesLocalEpisodes, $aTVSeriesGetSeriesLocalEpisodes
	Local $aTVSeriesFindSeriesVolumeLabelLocalEpisodes, $aTVSeriesGetSeriesVolumeLabelLocalEpisodes
	TVSeriesStatusUpdateReplaceLastRow("Wait 2/6... Getting all data from database table local_episodes to array, this takes some time!")
	$iReturnValue = _SQLite_GetTable2d(-1, "SELECT * FROM local_episodes ORDER BY SeriesID, VolumeLabel;", $aTVSeriesAllLocalEpisodes, $aTVSeriesAllLocalEpisodesRows, $aTVSeriesAllLocalEpisodesColumns)
	;ConsoleWrite("Array size: " & UBound($aTVSeriesSingleSeriesPaths) & " for " & $aTVSeriesSelectedPaths[$iCounterSelectedSeriesPaths - 1] & @CRLF)
	GUICtrlSetData($ProgressTVSeries, 100 * (2 / 6))

	If $iCancel = 1 Then
		; YES $IDYES (6)
		; NO $IDNO (7)
		TVSeriesStatusUpdateNewRow("Do you want to cancel?")
		$CancelMessageBox = MsgBox(4, "Cancel?", "Do you want to cancel?")
		;ConsoleWrite("box = " & $CancelMessageBox)
		If $CancelMessageBox = 6 Then
			_SQLite_Close($hDskDb)                     ; DB is a regular file that could be reopened later
			_SQLite_Shutdown()

			TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = Yes.")
			$aMovingPicturesLocalMedia = 0

			;Enable Delete radio button again...
			GUICtrlSetState($RadioTVSeriesDeleteMetadata, $GUI_ENABLE)

			GUICtrlSetState($RadioTVSeriesCreateMetadata, $GUI_ENABLE)
			GUICtrlSetState($LabelTVSeriesWorking, $GUI_HIDE)
			GUICtrlSetState($TabSheet1, $GUI_ENABLE)

			$aTVSeriesAllLocalEpisodes = 0
			$aTVSeriesAllActors = 0

			$iCancel = 0
			$TimerSet = 0
			;ExitLoop
			Return

		Else
			TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = No.")
			$iCancel = 0
		EndIf
	EndIf

	Local $aTVSeriesAllOnlineEpisodes, $aTVSeriesAllOnlineEpisodesRows, $aTVSeriesAllOnlineEpisodesColumns
	Local $aTVSeriesFindSeriesOnlineEpisodes, $aTVSeriesGetSeriesOnlineEpisodes
	TVSeriesStatusUpdateReplaceLastRow("Wait 3/6... Copying all data from database table online_episodes to array, this takes some time!")
	$iReturnValue = _SQLite_GetTable2d(-1, "SELECT * FROM online_episodes ORDER BY SeriesID;", $aTVSeriesAllOnlineEpisodes, $aTVSeriesAllOnlineEpisodesRows, $aTVSeriesAllOnlineEpisodesColumns)
	;ConsoleWrite("Array size: " & UBound($aTVSeriesSingleSeriesPaths) & " for " & $aTVSeriesSelectedPaths[$iCounterSelectedSeriesPaths - 1] & @CRLF)
	GUICtrlSetData($ProgressTVSeries, 100 * (3 / 6))

	If $iCancel = 1 Then
		; YES $IDYES (6)
		; NO $IDNO (7)
		TVSeriesStatusUpdateNewRow("Do you want to cancel?")
		$CancelMessageBox = MsgBox(4, "Cancel?", "Do you want to cancel?")
		;ConsoleWrite("box = " & $CancelMessageBox)
		If $CancelMessageBox = 6 Then
			_SQLite_Close($hDskDb)                     ; DB is a regular file that could be reopened later
			_SQLite_Shutdown()

			TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = Yes.")
			$aMovingPicturesLocalMedia = 0

			;Enable Delete radio button again...
			GUICtrlSetState($RadioTVSeriesDeleteMetadata, $GUI_ENABLE)

			GUICtrlSetState($RadioTVSeriesCreateMetadata, $GUI_ENABLE)
			GUICtrlSetState($LabelTVSeriesWorking, $GUI_HIDE)
			GUICtrlSetState($TabSheet1, $GUI_ENABLE)

			$aTVSeriesAllOnlineEpisodes = 0
			$aTVSeriesAllLocalEpisodes = 0
			$aTVSeriesAllActors = 0

			$iCancel = 0
			$TimerSet = 0
			;ExitLoop
			Return

		Else
			TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = No.")
			$iCancel = 0
		EndIf
	EndIf

	Local $aTVSeriesAllOnline_series, $aTVSeriesAllOnline_seriesRows, $aTVSeriesAllOnline_seriesColumns
	Local $aTVSeriesFindOneSerie, $aTVSeriesGetOneSerie
	TVSeriesStatusUpdateReplaceLastRow("Wait 4/6... Getting all data from database table online_series to array (this may take some time)...")
	$iReturnValueMemoryOnline_series = _SQLite_GetTable2d(-1, "SELECT * FROM online_series ORDER BY ID;", $aTVSeriesAllOnline_series, $aTVSeriesAllOnline_seriesRows, $aTVSeriesAllOnline_seriesColumns)
	GUICtrlSetData($ProgressTVSeries, 100 * (4 / 6))

	If $iCancel = 1 Then
		; YES $IDYES (6)
		; NO $IDNO (7)
		TVSeriesStatusUpdateNewRow("Do you want to cancel?")
		$CancelMessageBox = MsgBox(4, "Cancel?", "Do you want to cancel?")
		;ConsoleWrite("box = " & $CancelMessageBox)
		If $CancelMessageBox = 6 Then
			_SQLite_Close($hDskDb)                     ; DB is a regular file that could be reopened later
			_SQLite_Shutdown()

			TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = Yes.")
			$aMovingPicturesLocalMedia = 0

			;Enable Delete radio button again...
			GUICtrlSetState($RadioTVSeriesDeleteMetadata, $GUI_ENABLE)

			GUICtrlSetState($RadioTVSeriesCreateMetadata, $GUI_ENABLE)
			GUICtrlSetState($LabelTVSeriesWorking, $GUI_HIDE)
			GUICtrlSetState($TabSheet1, $GUI_ENABLE)

			$aTVSeriesAllOnlineEpisodes = 0
			$aTVSeriesAllLocalEpisodes = 0
			$aTVSeriesAllActors = 0
			$aTVSeriesAllOnline_series = 0

			$iCancel = 0
			$TimerSet = 0
			;ExitLoop
			Return

		Else
			TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = No.")
			$iCancel = 0
		EndIf
	EndIf

	Local $aTVSeriesAllSeasons, $aTVSeriesAllSeasonsRows, $aTVSeriesAllSeasonsColumns
	Local $aTVSeriesFindOneSeason, $aTVSeriesGetOneSeason
	TVSeriesStatusUpdateReplaceLastRow("Wait 5/6... Getting all data from database table season to array (this may take some time)...")
	$iReturnValueMemorySeason = _SQLite_GetTable2d(-1, "SELECT * FROM season ORDER BY SeriesID;", $aTVSeriesAllSeasons, $aTVSeriesAllSeasonsRows, $aTVSeriesAllSeasonsColumns)
	GUICtrlSetData($ProgressTVSeries, 100 * (5 / 6))

	If $iCancel = 1 Then
		; YES $IDYES (6)
		; NO $IDNO (7)
		TVSeriesStatusUpdateNewRow("Do you want to cancel?")
		$CancelMessageBox = MsgBox(4, "Cancel?", "Do you want to cancel?")
		;ConsoleWrite("box = " & $CancelMessageBox)
		If $CancelMessageBox = 6 Then
			_SQLite_Close($hDskDb)                     ; DB is a regular file that could be reopened later
			_SQLite_Shutdown()

			TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = Yes.")
			$aMovingPicturesLocalMedia = 0

			;Enable Delete radio button again...
			GUICtrlSetState($RadioTVSeriesDeleteMetadata, $GUI_ENABLE)

			GUICtrlSetState($RadioTVSeriesCreateMetadata, $GUI_ENABLE)
			GUICtrlSetState($LabelTVSeriesWorking, $GUI_HIDE)
			GUICtrlSetState($TabSheet1, $GUI_ENABLE)

			$aTVSeriesAllOnlineEpisodes = 0
			$aTVSeriesAllLocalEpisodes = 0
			$aTVSeriesAllActors = 0
			$aTVSeriesAllOnline_series = 0
			$aTVSeriesAllSeasons = 0

			$iCancel = 0
			$TimerSet = 0
			;ExitLoop
			Return

		Else
			TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = No.")
			$iCancel = 0
		EndIf
	EndIf

	Local $aTVSeriesAllFanart, $aTVSeriesAllFanartRows, $aTVSeriesAllFanartColumns
	Local $aTVSeriesFindOneFanart, $aTVSeriesGetOneFanart
	TVSeriesStatusUpdateReplaceLastRow("Wait 6/6... Getting all data from database table Fanart to array (this may take some time)...")
	$iReturnValueMemoryFanart = _SQLite_GetTable2d(-1, "SELECT * FROM Fanart WHERE LocalPath LIKE 'fanart%' ORDER BY seriesID;", $aTVSeriesAllFanart, $aTVSeriesAllFanartRows, $aTVSeriesAllFanartColumns)
	GUICtrlSetData($ProgressTVSeries, 100 * (6 / 6))

	If $iCancel = 1 Then
		; YES $IDYES (6)
		; NO $IDNO (7)
		TVSeriesStatusUpdateNewRow("Do you want to cancel?")
		$CancelMessageBox = MsgBox(4, "Cancel?", "Do you want to cancel?")
		;ConsoleWrite("box = " & $CancelMessageBox)
		If $CancelMessageBox = 6 Then
			_SQLite_Close($hDskDb)                     ; DB is a regular file that could be reopened later
			_SQLite_Shutdown()

			TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = Yes.")
			$aMovingPicturesLocalMedia = 0

			;Enable Delete radio button again...
			GUICtrlSetState($RadioTVSeriesDeleteMetadata, $GUI_ENABLE)

			GUICtrlSetState($RadioTVSeriesCreateMetadata, $GUI_ENABLE)
			GUICtrlSetState($LabelTVSeriesWorking, $GUI_HIDE)
			GUICtrlSetState($TabSheet1, $GUI_ENABLE)

			$aTVSeriesAllOnlineEpisodes = 0
			$aTVSeriesAllLocalEpisodes = 0
			$aTVSeriesAllActors = 0
			$aTVSeriesAllOnline_series = 0
			$aTVSeriesAllSeasons = 0
			$aTVSeriesAllFanart = 0

			$iCancel = 0
			$TimerSet = 0
			;ExitLoop
			Return

		Else
			TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = No.")
			$iCancel = 0
		EndIf
	EndIf

	Global $aTVSeriesLocalSeries
	Local $iTVSeriesProcessCounter = 1

	GUICtrlSetData($LabelTVSeriesWorking, "Please wait, step 2/3 preparing (press x to cancel)...")
	TVSeriesStatusUpdateNewRow("Starting Processing folders... ")
	TVSeriesStatusUpdateNewRow("...")
	;Run this once for every checked folder
	;First we get the common folder for each series
	For $iCounterSelectedSeriesPaths = 1 To UBound($aTVSeriesSelectedPaths)

		;Get all series from one folder
		$iReturnValue = _SQLite_GetTable(-1, "SELECT DISTINCT SeriesID FROM local_episodes WHERE VolumeLabel = """ & $aTVSeriesSelectedPaths[$iCounterSelectedSeriesPaths - 1] & """;", $aTVSeriesLocalSeries, $iTVSeriesLocalSeriesRows, $iTVSeriesLocalSeriesColumns)
		If $iReturnValue = $SQLITE_OK Then
			;_ArrayDisplay($aTVSeriesLocalSeries)  ; For testing
			Local $aTVSeriesSingleSeriesPathTrim
			Local $aTVSeriesSingleSeriesPaths, $iTVSeriesSingleSeriesPathsRows, $iTVSeriesSingleSeriesPathsColumns
			Local $aTVSeriesSingleSeriesPathsAllInRAM, $iTVSeriesSingleSeriesPathsAllInRAMRows, $iTVSeriesSingleSeriesPathsAllInRAMColumns
			Global $aTVSeriesLocalSeriesCommonPath[$iTVSeriesLocalSeriesRows + 3]
			Local $aTVSeriesFindAllEpisodeRowsForOneSeries, $aTVSeriesFindAllEpisodesForOneSeries

			$iReturnValueMemory = _SQLite_GetTable2d(-1, "SELECT EpisodeFilename, SeriesID FROM local_episodes WHERE EpisodeFilename LIKE '" & $aTVSeriesSelectedPaths[$iCounterSelectedSeriesPaths - 1] & "%' ORDER BY SeriesID;", $aTVSeriesSingleSeriesPathsAllInRAM, $iTVSeriesSingleSeriesPathsAllInRAMRows, $iTVSeriesSingleSeriesPathsAllInRAMColumns)
			;_ArrayDisplay($aTVSeriesSingleSeriesPathsAllInRAM)
			;$aTVSeriesFindAllEpisodeRowsForOneSeries = _ArrayFindAll($aTVSeriesSingleSeriesPathsAllInRAM, "328634", Default, Default, Default, Default, 1)
			;_ArrayDisplay($aTVSeriesFindAllEpisodeRowsForOneSeries, "Found")
			;MsgBox(0,0,"From " & $aTVSeriesFindAllEpisodeRowsForOneSeries[0] & " to " & $aTVSeriesFindAllEpisodeRowsForOneSeries[UBound($aTVSeriesFindAllEpisodeRowsForOneSeries)-1])
			;$aTVSeriesFindAllEpisodesForOneSeries = _ArrayExtract($aTVSeriesSingleSeriesPathsAllInRAM,$aTVSeriesFindAllEpisodeRowsForOneSeries[0],$aTVSeriesFindAllEpisodeRowsForOneSeries[UBound($aTVSeriesFindAllEpisodeRowsForOneSeries)-1])
			;_ArrayDisplay($aTVSeriesFindAllEpisodesForOneSeries)

			For $CheckSeriesPath = 2 To $iTVSeriesLocalSeriesRows + 1
				$aTVSeriesFindAllEpisodeRowsForOneSeries = _ArrayFindAll($aTVSeriesSingleSeriesPathsAllInRAM, $aTVSeriesLocalSeries[$CheckSeriesPath], Default, Default, Default, Default, 1)
				$aTVSeriesFindAllEpisodesForOneSeries = _ArrayExtract($aTVSeriesSingleSeriesPathsAllInRAM, $aTVSeriesFindAllEpisodeRowsForOneSeries[0], $aTVSeriesFindAllEpisodeRowsForOneSeries[UBound($aTVSeriesFindAllEpisodeRowsForOneSeries) - 1], 0, 0)

				;$iReturnValue = _SQLite_GetTable(-1, "SELECT EpisodeFilename FROM local_episodes WHERE SeriesID = " & $aTVSeriesLocalSeries[$CheckSeriesPath] & " AND EpisodeFilename LIKE '" & $aTVSeriesSelectedPaths[$iCounterSelectedSeriesPaths - 1] & "%';", $aTVSeriesSingleSeriesPaths, $iTVSeriesSingleSeriesPathsRows, $iTVSeriesSingleSeriesPathsColumns)
				;_ArrayDisplay($aTVSeriesFindAllEpisodesForOneSeries)

				$aTVSeriesSingleSeriesPathTrim = StringSplit($aTVSeriesFindAllEpisodesForOneSeries[0], "\")
				;_ArrayDisplay($aTVSeriesSingleSeriesPathTrim)

				;$aTVSeriesSingleSeriesPathTrim = StringSplit($aTVSeriesSingleSeriesPaths[2], "\")
				Local $sAreAllEpisodePathsSame = "No"
				Local $iLowestNumberOfTrimParts = $aTVSeriesSingleSeriesPathTrim[0]
				Local $aTVSeriesSingleSeriesPathWithoutEpisodePart[UBound($aTVSeriesFindAllEpisodeRowsForOneSeries) + 2]
				;Local $aTVSeriesSingleSeriesPathWithoutEpisodePart[$iTVSeriesSingleSeriesPathsRows + 2]
				Local $iTrimPartsToUse = 1, $testRound = 1

				While StringCompare($sAreAllEpisodePathsSame, "Yes") <> 0 Or $iLowestNumberOfTrimParts = 1
					;Sleep(3000)
					;All same, if not proven wrong
					$sAreAllEpisodePathsSame = "Yes"
					;For $CheckEpisodePath = 2 To $iTVSeriesSingleSeriesPathsRows + 1
					For $CheckEpisodePath = 2 To UBound($aTVSeriesFindAllEpisodeRowsForOneSeries) + 1

						$aTVSeriesSingleSeriesPathTrim = StringSplit($aTVSeriesFindAllEpisodesForOneSeries[$CheckEpisodePath - 2], "\")
						;ConsoleWrite($aTVSeriesSingleSeriesPathTrim & @CRLF)
						;$aTVSeriesSingleSeriesPathTrim = StringSplit($aTVSeriesSingleSeriesPaths[$CheckEpisodePath], "\")
						;ConsoleWrite("Round " & $testRound & " ")

						If $testRound > 1 Then
							$iTrimPartsToUse = $iLowestNumberOfTrimParts
						Else
							$iTrimPartsToUse = $aTVSeriesSingleSeriesPathTrim[0] - 1

							If $iLowestNumberOfTrimParts > $aTVSeriesSingleSeriesPathTrim[0] Then
								$iLowestNumberOfTrimParts = $aTVSeriesSingleSeriesPathTrim[0]
							EndIf

						EndIf

						;_ArrayDisplay($aTVSeriesSingleSeriesPathTrim)

						$aTVSeriesSingleSeriesPathWithoutEpisodePart[$CheckEpisodePath] = ""
						For $ThisPathPart = 1 To $iTrimPartsToUse
							$aTVSeriesSingleSeriesPathWithoutEpisodePart[$CheckEpisodePath] = $aTVSeriesSingleSeriesPathWithoutEpisodePart[$CheckEpisodePath] & $aTVSeriesSingleSeriesPathTrim[$ThisPathPart] & "\"
						Next

						If StringCompare($aTVSeriesSelectedPaths[$iCounterSelectedSeriesPaths - 1], $aTVSeriesSingleSeriesPathWithoutEpisodePart[$CheckEpisodePath]) = 0 Then
							;If true, series path is same as scrubbing path
							$aTVSeriesLocalSeriesCommonPath[$CheckSeriesPath] = $aTVSeriesSingleSeriesPathWithoutEpisodePart[$CheckEpisodePath]
							$sAreAllEpisodePathsSame = "Yes"
						ElseIf $CheckEpisodePath > 2 And $sAreAllEpisodePathsSame <> "No" Then
							If StringCompare($aTVSeriesSingleSeriesPathWithoutEpisodePart[$CheckEpisodePath], $aTVSeriesSingleSeriesPathWithoutEpisodePart[$CheckEpisodePath - 1]) = 0 Then
								$sAreAllEpisodePathsSame = "Yes"
							Else
								$sAreAllEpisodePathsSame = "No"
							EndIf
						EndIf

						;ConsoleWrite($aTVSeriesSingleSeriesPathWithoutEpisodePart[$CheckEpisodePath] & " Same = " & $sAreAllEpisodePathsSame & " min parts " & $iLowestNumberOfTrimParts & @CRLF)

						If $iCancel = 1 Then
							; YES $IDYES (6)
							; NO $IDNO (7)
							TVSeriesStatusUpdateNewRow("Do you want to cancel?")
							$CancelMessageBox = MsgBox(4, "Cancel?", "Do you want to cancel?")
							;ConsoleWrite("box = " & $CancelMessageBox)
							If $CancelMessageBox = 6 Then
								_SQLite_Close($hDskDb) ; DB is a regular file that could be reopened later
								_SQLite_Shutdown()

								TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = Yes.")
								$aMovingPicturesLocalMedia = 0

								;Enable Delete radio button again...
								GUICtrlSetState($RadioTVSeriesDeleteMetadata, $GUI_ENABLE)

								GUICtrlSetState($RadioTVSeriesCreateMetadata, $GUI_ENABLE)
								GUICtrlSetState($LabelTVSeriesWorking, $GUI_HIDE)
								GUICtrlSetState($TabSheet1, $GUI_ENABLE)

								$aTVSeriesAllOnlineEpisodes = 0
								$aTVSeriesAllLocalEpisodes = 0
								$aTVSeriesAllActors = 0
								$aTVSeriesAllOnline_series = 0
								$aTVSeriesAllSeasons = 0
								$aTVSeriesAllFanart = 0

								$iCancel = 0
								$TimerSet = 0
								;ExitLoop
								Return

							Else
								TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = No.")
								$iCancel = 0
							EndIf
						EndIf

					Next
					$testRound += 1
					$iLowestNumberOfTrimParts -= 1

					If $iLowestNumberOfTrimParts = 1 Then ExitLoop
					If StringCompare($sAreAllEpisodePathsSame, "Yes") = 0 Then ExitLoop
					;ExitLoop
				WEnd

				;If all have same path = series path
				If $sAreAllEpisodePathsSame = "Yes" Then
					$aTVSeriesLocalSeriesCommonPath[$CheckSeriesPath] = $aTVSeriesSingleSeriesPathWithoutEpisodePart[$CheckEpisodePath - 1]
				EndIf

				;ConsoleWrite("MEM Series ID: " & $aTVSeriesLocalSeries[$CheckSeriesPath] & " Common path = " & $aTVSeriesLocalSeriesCommonPath[$CheckSeriesPath] & @CRLF)


				Local $sNewStatusInfo = "On folder: " & $iCounterSelectedSeriesPaths & "/" & UBound($aTVSeriesSelectedPaths) & ". "
				$sNewStatusInfo = $sNewStatusInfo & "First step (the fast step): Get common series folders " & $CheckSeriesPath - 1 & "/" & $iTVSeriesLocalSeriesRows & " "
				TVSeriesStatusUpdateReplaceLastRow($sNewStatusInfo)
				GUICtrlSetData($ProgressTVSeries, 100 * (($CheckSeriesPath - 1) / $iTVSeriesLocalSeriesRows))

			Next

		Else
			MsgBox($MB_SYSTEMMODAL, "SQLite Error: " & $iReturnValue, _SQLite_ErrMsg())
		EndIf


;~ 	Now we have the common folder for each series in this "SeriesPath"
;~  Series ID: $aTVSeriesLocalSeries[$CheckSeriesPath] --> Common path = $aTVSeriesLocalSeriesCommonPath[$CheckSeriesPath]

		GUICtrlSetData($LabelTVSeriesWorking, "Please wait, step 3/3 working (press x to cancel)...")

		TVSeriesStatusUpdateNewRow("Creating  and copying series files...")

		For $iSeries = 2 To UBound($aTVSeriesLocalSeries) - 1

			If $iCancel = 1 Then
				; YES $IDYES (6)
				; NO $IDNO (7)
				TVSeriesStatusUpdateNewRow("Do you want to cancel?")
				$CancelMessageBox = MsgBox(4, "Cancel?", "Do you want to cancel?")
				;ConsoleWrite("box = " & $CancelMessageBox)
				If $CancelMessageBox = 6 Then
					_SQLite_Close($hDskDb)             ; DB is a regular file that could be reopened later
					_SQLite_Shutdown()

					TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = Yes.")
					$aMovingPicturesLocalMedia = 0

					;Enable Delete radio button again...
					GUICtrlSetState($RadioTVSeriesDeleteMetadata, $GUI_ENABLE)

					GUICtrlSetState($RadioTVSeriesCreateMetadata, $GUI_ENABLE)
					GUICtrlSetState($LabelTVSeriesWorking, $GUI_HIDE)
					GUICtrlSetState($TabSheet1, $GUI_ENABLE)

					$aTVSeriesAllOnlineEpisodes = 0
					$aTVSeriesAllLocalEpisodes = 0
					$aTVSeriesAllActors = 0
					$aTVSeriesAllOnline_series = 0
					$aTVSeriesAllSeasons = 0
					$aTVSeriesAllFanart = 0

					$iCancel = 0
					$TimerSet = 0
					;ExitLoop
					Return

				Else
					TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = No.")
					$iCancel = 0
				EndIf
			EndIf

			GUICtrlSetData($LabelTVSeriesLogSeriesRow0, GUICtrlRead($LabelTVSeriesLogSeriesRow1))
			GUICtrlSetData($LabelTVSeriesLogSeriesRow1, GUICtrlRead($LabelTVSeriesLogSeriesRow2))
			GUICtrlSetData($LabelTVSeriesLogSeriesRow2, "Series no: " & $iSeries - 1)
			GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & " ID: " & $aTVSeriesLocalSeries[$iSeries])

			TVSeriesStatusUpdateReplaceLastRow("Creating  and copying series files " & $iSeries - 1 & "/" & UBound($aTVSeriesLocalSeries) - 2)

			;Get all info for current serie = $aTVSeriesGetOneSerie
			$aTVSeriesFindOneSerie = _ArrayFindAll($aTVSeriesAllOnline_series, $aTVSeriesLocalSeries[$iSeries], Default, Default, Default, Default, 0)
			$aTVSeriesGetOneSerie = _ArrayExtract($aTVSeriesAllOnline_series, $aTVSeriesFindOneSerie[0], $aTVSeriesFindOneSerie[UBound($aTVSeriesFindOneSerie) - 1])
			;_ArrayDisplay($aTVSeriesGetOneSerie)

			;Get all actors for current serie = $aTVSeriesGetAllActorsForOneSeries
			$aTVSeriesFindAllActorsForOneSeries = _ArrayFindAll($aTVSeriesAllActors, $aTVSeriesLocalSeries[$iSeries], Default, Default, Default, Default, 1)
			If $aTVSeriesFindAllActorsForOneSeries = -1 Then
				;Make a fake actor

			Else     ; All ok
				$aTVSeriesGetAllActorsForOneSeries = _ArrayExtract($aTVSeriesAllActors, $aTVSeriesFindAllActorsForOneSeries[0], $aTVSeriesFindAllActorsForOneSeries[UBound($aTVSeriesFindAllActorsForOneSeries) - 1])
				;_ArrayDisplay($aTVSeriesGetAllActorsForOneSeries)
			EndIf

			GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & " " & StringLeft($aTVSeriesGetOneSerie[0][1], 30) & "...")
			If FileExists($aTVSeriesLocalSeriesCommonPath[$iSeries] & "tvshow.nfo") And $iReplaceFiles = 0 Then
				GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", no nfo")
				; Do nothing
				;ConsoleWrite("NFO Doing nothing! f exist & no rpls " & $aTVSeriesLocalSeriesCommonPath[$iSeries] & "tvshow.nfo" & " " & GUICtrlRead($CheckboxTVSeriesReplaceExistingFiles) & @CRLF) ; Used for testing

			ElseIf GUICtrlRead($CheckboxTVSeriesCreateSeriesNfo) = 1 Then

				;ConsoleWrite("SeriesID: " & $aTVSeriesLocalSeries[$iSeries] & " > Common Path: " & $aTVSeriesLocalSeriesCommonPath[$iSeries] & @CRLF)

				;Do this
				; Open the file for writing (append to the end of a file) and store the handle to a variable.
				Local $hFileOpen = FileOpen($aTVSeriesLocalSeriesCommonPath[$iSeries] & "tvshow.nfo", 2)
				If $hFileOpen = -1 Then
					MsgBox($MB_SYSTEMMODAL, "", "An error occurred whilst writing the tvshow.nfo temporary file.")
					Return False
				EndIf

;~ 				Should I use standalone="yes"?
;~ 				<?xml version="1.0" encoding="UTF-8" standalone="yes"?>

;~ ; FILE			XML		TABLE			COLUMN	NAME				NOTE
;~ ;tvshow.nfo<actor><name>	Actors			3		Name				Example, India Elsley
;~ ;tvshow.nfo<actor><role>	Actors			4		Role				Example, Fauna Hodel
;~ ;tvshow.nfo <id>			online_series	0		ID					Example, 349997
;~ ;tvshow.nfo <tvdbid>		online_series	0		ID					Example, 349997
;~ ;tvshow.nfo <title>		online_series	1		Pretty_Name			Example, I Am the Night
;~ ;tvshow.nfo <sorttitle>	online_series	2		SortName			Example, I Am the Night
;~ ;tvshow.nfo <status>		online_series	4		Status				Example, Continuing
;~ ;tvshow.nfo <genre>		online_series	5		Genre				Example, |Drama|Mini-Series|Mystery|
;~ ;clearlogo.jpg			online_series	7		CurrentBannerFileName Example, I Am the Night\-langen-graphical/5c20fb9a27701.jpg
;~ ;poster.jpg				online_series	9		PosterBannerFileName Example, I Am the Night\-langen-posters/5c23b2ae1af58.jpg
;~ ;tvshow.nfo <plot>		online_series	10		Summary				Example, Fauna Hodel is a...
;~ ;tvshow.nfo <premiered>	online_series	30		FirstAired			Example, 2019-03-04
;~ ;tvshow.nfo <year> 		online_series	30		FirstAired			Example, 2019-03-04
;~ ;tvshow.nfo <imdbid>		online_series	31		IMDB_ID				Example, tt7186588
;~ ;tvshow.nfo <studio>		online_series	34		Network				Example, TNT (US)
;~ ;tvshow.nfo <rating>		online_series	36		Rating				Example, 7
;~ ;tvshow.nfo <votes>		online_series	37		RatingCount			Example, 34
;~ ;clearart.jpg			Fanart			4		BannerPath			Example, fanart/original/5c0857a8645b0.jpg
;~ ;season01-poster.jpg		season			4		CurrentBannerFileName Example, Better Off Ted\-langen-seasons/84021-1-2.jpg
;~ ;
;~ ; ALL PICTURES
;~ ;clearart.jpg			Fanart			4		BannerPath			Example, fanart/original/5c0857a8645b0.jpg
;~ ;fanart.jpg = better name? Or both?
;~ ;clearlogo.jpg			online_series	7		CurrentBannerFileName Example, I Am the Night\-langen-graphical/5c20fb9a27701.jpg
;~ ;banner.jpg = better name? Or both?
;~ ;poster.jpg				online_series	9		PosterBannerFileName Example, I Am the Night\-langen-posters/5c23b2ae1af58.jpg
;~ ;season01-poster.jpg		season			4		CurrentBannerFileName Example, Better Off Ted\-langen-seasons/84021-1-2.jpg

;~  Lets create tvshow.nfo files and copy some pics

				;TVSeriesStatusUpdateMoreOnLastRow(" Nfo ")

				GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", nfo")
				; Write data to the file using the handle returned by FileOpen.
				;ConsoleWrite("NFO Doing something!" & @CRLF) ; Used for testing
				Local $sTextToFile = "<?xml version=""1.0"" encoding=""UTF-8""?>" & @CRLF
				$sTextToFile = $sTextToFile & "<!--created on " & @YEAR & "-" & @MON & "-" & @MDAY & " " & @HOUR & ":" & @MIN & ":" & @SEC & " -->" & @CRLF
				$sTextToFile = $sTextToFile & "<tvshow>" & @CRLF
				$sTextToFile = $sTextToFile & "  <id>" & $aTVSeriesLocalSeries[$iSeries] & "</id>" & @CRLF
				$sTextToFile = $sTextToFile & "  <title>" & $aTVSeriesGetOneSerie[0][1] & "</title>" & @CRLF
				$sTextToFile = $sTextToFile & "  <sorttitle>" & $aTVSeriesGetOneSerie[0][2] & "</sorttitle>" & @CRLF
				$sTextToFile = $sTextToFile & "  <plot>" & $aTVSeriesGetOneSerie[0][10] & "</plot>" & @CRLF
				$sTextToFile = $sTextToFile & "  <premiered>" & $aTVSeriesGetOneSerie[0][30] & "</premiered>" & @CRLF
				$sTextToFile = $sTextToFile & "  <year>" & StringLeft($aTVSeriesGetOneSerie[0][30], 4) & "</year>" & @CRLF
				$sTextToFile = $sTextToFile & "  <rating>" & $aTVSeriesGetOneSerie[0][36] & "</rating>" & @CRLF
				$sTextToFile = $sTextToFile & "  <votes>" & $aTVSeriesGetOneSerie[0][37] & "</votes>" & @CRLF
				$sTextToFile = $sTextToFile & "  <tvdbid>" & $aTVSeriesGetOneSerie[0][0] & "</tvdbid>" & @CRLF
				$sTextToFile = $sTextToFile & "  <imdbid>" & $aTVSeriesGetOneSerie[0][31] & "</imdbid>" & @CRLF
				$sTextToFile = $sTextToFile & "  <status>" & $aTVSeriesGetOneSerie[0][4] & "</status>" & @CRLF
				$sTextToFile = $sTextToFile & MultiSingleLine2MultiXmlRows("  <genre>", $aTVSeriesGetOneSerie[0][5], "</genre>")
				$sTextToFile = $sTextToFile & "  <studio>" & $aTVSeriesGetOneSerie[0][34] & "</studio>" & @CRLF
				For $iThisActor = 0 To UBound($aTVSeriesGetAllActorsForOneSeries) - 1
					$sTextToFile = $sTextToFile & "  <actor>" & @CRLF & "    <name>" & $aTVSeriesGetAllActorsForOneSeries[$iThisActor][3] & "</name>" & @CRLF
					$sTextToFile = $sTextToFile & "    <role>" & $aTVSeriesGetAllActorsForOneSeries[$iThisActor][4] & "</role>" & @CRLF & "  </actor>" & @CRLF
				Next
				$sTextToFile = $sTextToFile & "</tvshow>"
				FileWrite($hFileOpen, $sTextToFile)

				; Close the handle returned by FileOpen.
				FileClose($hFileOpen)

				If GUICtrlRead($CheckboxTVSeriesSameTimestampAsMedia) = 1 Then
					;In this case, do nothing
					;FileSetTime($aTVSeriesLocalSeriesCommonPath[$iSeries] & "tvshow.nfo", FileGetTime($aMovingPicturesLocalMedia[$y][2], $FT_MODIFIED, $FT_STRING), $FT_MODIFIED)
				EndIf



			Else
				; Do nothing
				;ConsoleWrite("NFO Doing nothing! ChkBx .nfo" & GUICtrlRead($CheckboxTVSeriesCreateSeriesNfo) & @CRLF) ; Used for testing
			EndIf

;~ ; ALL PICTURES
;~ ;clearart.jpg			Fanart			3		LocalPath			Example, fanart/original/5c0857a8645b0.jpg
;~ ;fanart.jpg = better name? Or both?

;~ ;season01-poster.jpg		season			4		CurrentBannerFileName Example, Better Off Ted\-langen-seasons/84021-1-2.jpg

;~ ;clearlogo.jpg			online_series	7		CurrentBannerFileName Example, I Am the Night\-langen-graphical/5c20fb9a27701.jpg
;~ ;banner.jpg = better name? Or both?

;~ ;poster.jpg				online_series	9		PosterBannerFileName Example, I Am the Night\-langen-posters/5c23b2ae1af58.jpg


			;Copy serie pictures

			; POSTER
			; HERE WE START COPYING THE poster.jpg FILE
			If FileExists($aTVSeriesLocalSeriesCommonPath[$iSeries] & "poster.jpg") And $iReplaceFiles = 0 Then
				;Do nothing
				GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", no poster")
			ElseIf GUICtrlRead($CheckboxTVSeriesCopySeriesPoster) = 1 Then
				GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", poster")
				;Do this
				;We alreade have online_series info for current serie in $aTVSeriesGetOneSerie

				;TVSeriesStatusUpdateMoreOnLastRow(" Poster ")

				Local $sFileToCopy, $sFileToCreate, $sFileCopyINfo

				$sFileToCopy = $sTVSeriesProgramDataFolder & "\Thumbs\MPTVSeriesBanners\" & StringReplace($aTVSeriesGetOneSerie[0][9], "/", "\")

				$sFileToCreate = $aTVSeriesLocalSeriesCommonPath[$iSeries] & "poster.jpg"

				$sFileCopyINfo = FileCopy($sFileToCopy, $sFileToCreate, $iReplaceFiles)

				;ConsoleWrite($sFileCopyINfo & " " & $sFileToCopy & " > " & $sFileToCreate & @CRLF)

				;If GUICtrlRead($CheckboxMovingPicturesSameTimestampAsMedia) = 1 Then
				;FileSetTime($sFileNameWithoutExtension & "-fanart.jpg", FileGetTime($aMovingPicturesLocalMedia[$y][2], $FT_MODIFIED, $FT_STRING), $FT_MODIFIED)
				;EndIf

			Else
				; Do nothing
				GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", no poster")
			EndIf

			; BANNER / CLEARLOGO
			; HERE WE START COPYING THE banner.jpg FILE
			If FileExists($aTVSeriesLocalSeriesCommonPath[$iSeries] & "banner.jpg") And $iReplaceFiles = 0 Then
				; Do nothing
				GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", no banner")
			ElseIf GUICtrlRead($CheckboxTVSeriesCopySeriesBanner) = 1 Then
				GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", banner")
				;Do this
				;We alreade have online_series info for current serie in $aTVSeriesGetOneSerie

				;TVSeriesStatusUpdateMoreOnLastRow(" Banner ")

				Local $sFileToCopy, $sFileToCreate, $sFileCopyINfo

				$sFileToCopy = $sTVSeriesProgramDataFolder & "\Thumbs\MPTVSeriesBanners\" & StringReplace($aTVSeriesGetOneSerie[0][7], "/", "\")

				$sFileToCreate = $aTVSeriesLocalSeriesCommonPath[$iSeries] & "banner.jpg"

				$sFileCopyINfo = FileCopy($sFileToCopy, $sFileToCreate, $iReplaceFiles)

				;ConsoleWrite($sFileCopyINfo & " " & $sFileToCopy & " > " & $sFileToCreate & @CRLF)

				;If GUICtrlRead($CheckboxMovingPicturesSameTimestampAsMedia) = 1 Then
				;FileSetTime($sFileNameWithoutExtension & "-fanart.jpg", FileGetTime($aMovingPicturesLocalMedia[$y][2], $FT_MODIFIED, $FT_STRING), $FT_MODIFIED)
				;EndIf

			Else
				; Do nothing
				GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", no banner")
			EndIf

			; SEASON
			; HERE WE START COPYING THE seasonXX-poster.jpg FILES


			If GUICtrlRead($CheckboxTVSeriesCopySeriesSeasonPoster) = 1 Then
				GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", s. poster")
				;Do this

				;TVSeriesStatusUpdateMoreOnLastRow(" Season posters ")

				$aTVSeriesFindOneSeason = _ArrayFindAll($aTVSeriesAllSeasons, $aTVSeriesLocalSeries[$iSeries], Default, Default, Default, Default, 1)
				$aTVSeriesGetOneSeason = _ArrayExtract($aTVSeriesAllSeasons, $aTVSeriesFindOneSeason[0], $aTVSeriesFindOneSeason[UBound($aTVSeriesFindOneSeason) - 1])

				Local $iCounterNumber = "", $sFileToCopy, $sFileToCreate, $sFileCopyINfo

				For $iThisPart = 0 To UBound($aTVSeriesGetOneSeason) - 1

					If Number($aTVSeriesGetOneSeason[$iThisPart][2]) < 9 Then
						$iCounterNumber = "0" & $aTVSeriesGetOneSeason[$iThisPart][2]
					Else
						$iCounterNumber = $aTVSeriesGetOneSeason[$iThisPart][2]
					EndIf

					$sFileToCopy = $sTVSeriesProgramDataFolder & "\Thumbs\MPTVSeriesBanners\" & StringReplace($aTVSeriesGetOneSeason[$iThisPart][4], "/", "\")

					$sFileToCreate = $aTVSeriesLocalSeriesCommonPath[$iSeries] & "season" & $iCounterNumber & "-poster.jpg"

					If FileExists($sFileToCreate) And $iReplaceFiles = 0 Then
						; Do nothing
						GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", no" & $iThisPart)
					Else
						$sFileCopyINfo = FileCopy($sFileToCopy, $sFileToCreate, $iReplaceFiles)
						GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", " & $iThisPart)
					EndIf

					;ConsoleWrite($sFileCopyINfo & " " & $sFileToCopy & " > " & $sFileToCreate & @CRLF)

					;If GUICtrlRead($CheckboxMovingPicturesSameTimestampAsMedia) = 1 Then
					;FileSetTime($sFileNameWithoutExtension & "-fanart.jpg", FileGetTime($aMovingPicturesLocalMedia[$y][2], $FT_MODIFIED, $FT_STRING), $FT_MODIFIED)
					;EndIf
				Next
			Else
				; Do nothing
				GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", no s. poster")
			EndIf

			; FANART / CLEARART
			; HERE WE START COPYING THE fanart.jpg FILES
			If GUICtrlRead($CheckboxTVSeriesCopySeriesFanart) = 1 Then
				GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", fanart")
				;Do this

				;TVSeriesStatusUpdateMoreOnLastRow(" Fanart ")

				$aTVSeriesFindOneFanart = _ArrayFindAll($aTVSeriesAllFanart, $aTVSeriesLocalSeries[$iSeries], Default, Default, Default, Default, 1)
				If $aTVSeriesFindOneFanart = -1 Then
					;Fake it
				Else
					$aTVSeriesGetOneFanart = _ArrayExtract($aTVSeriesAllFanart, $aTVSeriesFindOneFanart[0], $aTVSeriesFindOneFanart[UBound($aTVSeriesFindOneFanart) - 1])
				EndIf

				Local $iFanartNumber = "", $sFileToCopy, $sFileToCreate, $sFileCopyINfo

				For $iThisFanart = 0 To UBound($aTVSeriesGetOneFanart) - 1

					If $iThisFanart = 0 Then
						$iFanartNumber = ""
					Else
						$iFanartNumber = $iThisFanart
					EndIf

					$sFileToCopy = $sTVSeriesProgramDataFolder & "\Thumbs\Fan Art\" & $aTVSeriesGetOneFanart[$iThisFanart][3]

					$sFileToCreate = $aTVSeriesLocalSeriesCommonPath[$iSeries] & "fanart" & $iFanartNumber & ".jpg"

					If FileExists($sFileToCreate) And $iReplaceFiles = 0 Then
						; Do nothing
						GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", no" & $iThisFanart)
					Else
						$sFileCopyINfo = FileCopy($sFileToCopy, $sFileToCreate, $iReplaceFiles)
						GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", " & $iThisFanart)
					EndIf

					;ConsoleWrite($sFileCopyINfo & " " & $sFileToCopy & " > " & $sFileToCreate & @CRLF)

					;If GUICtrlRead($CheckboxMovingPicturesSameTimestampAsMedia) = 1 Then
					;FileSetTime($sFileNameWithoutExtension & "-fanart.jpg", FileGetTime($aMovingPicturesLocalMedia[$y][2], $FT_MODIFIED, $FT_STRING), $FT_MODIFIED)
					;EndIf
				Next
			Else
				; Do nothing
				GUICtrlSetData($LabelTVSeriesLogSeriesRow2, GUICtrlRead($LabelTVSeriesLogSeriesRow2) & ", no fanart")
			EndIf



;~  Time for the loop for episodes

;~ 			For one series (from one user selected path) we get to $iReturnValue all episodes from local_episodesTVSeriesStatusUpdateReplaceLastRow("Wait... Getting all episodes " & $CheckSeriesPath - 1 + ($iCounterSelectedSeriesPaths - 1) * $iTVSeriesLocalSeriesRows & "/" & UBound($aTVSeriesSelectedPaths) * $iTVSeriesLocalSeriesRows)

;~ 			OK, let's go through all the files in the folder
;~ 			The rows and columns are found like this $aTVSeriesLocalEpisodes[rows][columns]
;~ 			NOTE: First row (number 0) in $aTVSeriesLocalEpisodes contains headersl, so we start with 1
;~ 			; Info from
;~ 			; https://kodi.wiki/view/TV-Show_artwork
;~ 			; https://www.team-mediaportal.com/wiki/display/glossary/NFO+Node+-+Lexical+support
;~ 			; https://www.team-mediaportal.com/wiki/display/MediaPortal2/NfoSeriesMetadataExtractor
;~ 			; https://kodi.wiki/view/NFO_files/TV_shows

;~ 						Tables we will use are, and some important columns
;~  FILE			XML		TABLE			COLUMN	NAME				NOTE
;~ 			 				local_episodes	0		EpisodeFilename		Example, Full path and name
;~ 			 				local_episodes	2		CompositeID			Example, 349997_1x2
;~ epi.nfo <id> 			local_episodes	3		SeriesID			Example, 349997
;~ epi.nfo <season>			local_episodes	4		SeasonIndex			Example, 2 (season 2)
;~ epi.nfo <episode>		local_episodes	5		EpisodeIndex		Example, 4 (episode 4)
;~ epi.nfo <title> No 2		local_episodes	6		LocalEpisodeName	Example, Pilot
;~ epi.nfo <year> No 2		local_episodes	16		FileDateCreated		Example, 2018-05-12
;~ 							local_episodes	19		VolumeLabel			Full Path
;~ 							local_episodes	36		DateWatched			Example, 2017-05-25 21:54:04
;~ 							Actors			1		SeriesID			Example, 349997
;~ 							Actors			2		Image				Example, actors/517070.jpg
;~ tvshow.nfo<actor><name>	Actors			3		Name				Example, India Elsley
;~ epi.nfo <actor><name> 	Actors			3		Name				Example, India Elsley
;~ tvshow.nfo<actor><role>	Actors			4		Role				Example, Fauna Hodel
;~ epi.nfo <actor><role> 	Actors			4		Role				Example, Fauna Hodel
;~ 							Fanart			1		SeriesID			Example, 349997
;~ 							Fanart			3		LocalPath			Example, fanart\original\349997-5c0857a8645b0.jpg
;~ 	clearart.jpg			Fanart			4		BannerPath			Example, fanart/original/5c0857a8645b0.jpg
;~ 							Fanart			5		ThumbnailPath		Example, _cache/fanart/original/5c0857a8645b0.jpg
;~ 							Fanart			8		SeriesName			Example, false
;~ 							Fanart			12		Rating				Example, 7.5
;~ 							Fanart			14		VignettePath		Example, fanart/vignette/5c0857a8645b0.jpg
;~ 	 						local_series	0		Parsed_Name			Example, I Am The Night
;~ 	 						local_series	1		ID					Example, 349997
;~ 							local_series	2		ScanIgnore			Example, 0 or 1 (1 = dublicate name)
;~ 							online_episodes	0		CompositeID			Example, 349997_1x2
;~ 							online_episodes	1		EpisodeID			Example, 6942262
;~ epi.nfo <tvdbid>			online_episodes	2		SeriesID			Example, 349997
;~ epi.nfo <episode>		online_episodes	3		EpisodeIndex		Example, 1
;~ epi.nfo <season>			online_episodes	4		SeasonIndex			Example, 1
;~ epi.nfo <title> No 1		online_episodes	5		EpisodeName			Example, Phenomenon of Interference
;~ (epi.nfo <watched>)		online_episodes	6		Watched				Example, 0 or 1 (1 = yes) > false/true in XML
;~ epi.nfo <plot>			online_episodes	7		Summary				Example, Fauna attempts to...
;~ epi.nfo <year> No 1		online_episodes	8		FirstAired			Example, 2019-02-04
;~ 							online_episodes	17		ThumbURL			Example, episodes/349997/6942262.jpg
;~ 							online_episodes	18		thumbFilename		Example, I Am the Night\Episodes\1x2.jpg
;~ epi.nfo <dvd_episode>	online_episodes	23		DVD_episodenumber	Example, 2
;~ epi.nfo <dvd_season>		online_episodes	24		DVD_season			Example, 2
;~ epi.nfo <imdbid>			online_episodes	26		IMDB_ID				Example, tt7660656
;~ epi.nfo <language>		online_episodes	27		Language			Example, en
;~ epi.nfo <rating>			online_episodes	29		Raiting				Example, 7
;~ epi.nfo <votes>			online_episodes	30		RaitingCount		Example, 34
;~ 							online_episodes	32		seasonid			Example, 787578
;~ epi.nfo <playcount>		online_episodes	53		PlayCount			Example, 2
;~ tvshow.nfo <id>			online_series	0		ID					Example, 349997
;~ tvshow.nfo <tvdbid>		online_series	0		ID					Example, 349997
;~ tvshow.nfo <title>		online_series	1		Pretty_Name			Example, I Am the Night
;~ epi.nfo <showtitle>		online_series	1		Pretty_Name			Example, I Am the Night
;~ tvshow.nfo <sorttitle>	online_series	2		SortName			Example, I Am the Night
;~ 							online_series	3		origName			Example,
;~ tvshow.nfo <status>		online_series	4		Status				Example, Continuing
;~ epi.nfo <status>			online_series	4		Status				Example, Continuing
;~ tvshow.nfo <genre>		online_series	5		Genre				Example, |Drama|Mini-Series|Mystery|
;~ epi.nfo <genre>			online_series	5		Genre				Example, |Drama|Mini-Series|Mystery|
;~ 							online_series	6		BannerFileNames		Example, I Am the Night\-langen-graphical/5c20fb9a27701.jpg
;~ clearlogo.jpg			online_series	7		CurrentBannerFileName Example, I Am the Night\-langen-graphical/5c20fb9a27701.jpg
;~ 							online_series	8		PosterFileNames		Example, \The Good Place\-langen-posters/311711-1.jpg|\The Good Place\-langen-posters/311711-2.jpg|\The Good Place\-langen-posters/311711-4.jpg|\The Good Place\-langen-posters/311711-3.jpg|
;~ poster.jpg				online_series	9		PosterBannerFileName Example, I Am the Night\-langen-posters/5c23b2ae1af58.jpg
;~ tvshow.nfo <plot>		online_series	10		Summary				Example, Fauna Hodel is a...
;~ 							online_series	29		banner				Example, graphical/5c20fb9a27701.jpg
;~ tvshow.nfo <premiered>	online_series	30		FirstAired			Example, 2019-03-04
;~ tvshow.nfo <year> 		online_series	30		FirstAired			Example, 2019-03-04
;~ tvshow.nfo <imdbid>		online_series	31		IMDB_ID				Example, tt7186588
;~ 							online_series	33		ContentRating		Example, TV-MA
;~ tvshow.nfo <studio>		online_series	34		Network				Example, TNT (US)
;~ epi.nfo <studio>			online_series	34		Network				Example, TNT (US)
;~ tvshow.nfo <rating>		online_series	36		Rating				Example, 7
;~ tvshow.nfo <votes>		online_series	37		RatingCount			Example, 34
;~ 							online_series	42		fanart				Example, fanart/original/5c0857a8645b0.jpg
;~ 							online_series	44		poster				Example, posters/5c23b2ae1af58.jpg
;~ 							season			1		SeriesID			Example, 349997
;~ 							season			2		SeasonIndex			Example, 1
;~ 							season			3		BannerFileNames		Example, Better Off Ted\-langen-seasons/84021-1-2.jpg|Better Off Ted\-langen-seasons/84021-1.jpg
;~ season01-poster.jpg		season			4		CurrentBannerFileName Example, Better Off Ted\-langen-seasons/84021-1-2.jpg
;~
;~

			; EPISODE NFO
			; HERE WE START CREATING THE EPISODE NFO FILE

			$aTVSeriesFindSeriesOnlineEpisodes = _ArrayFindAll($aTVSeriesAllOnlineEpisodes, $aTVSeriesLocalSeries[$iSeries], Default, Default, Default, Default, 2)
			If $aTVSeriesFindSeriesOnlineEpisodes = -1 Then
				;Make a fake episode

			Else     ; All ok
				$aTVSeriesGetSeriesOnlineEpisodes = _ArrayExtract($aTVSeriesAllOnlineEpisodes, $aTVSeriesFindSeriesOnlineEpisodes[0], $aTVSeriesFindSeriesOnlineEpisodes[UBound($aTVSeriesFindSeriesOnlineEpisodes) - 1])
			EndIf

			$aTVSeriesFindSeriesLocalEpisodes = _ArrayFindAll($aTVSeriesAllLocalEpisodes, $aTVSeriesLocalSeries[$iSeries], Default, Default, Default, Default, 3)
			$aTVSeriesGetSeriesLocalEpisodes = _ArrayExtract($aTVSeriesAllLocalEpisodes, $aTVSeriesFindSeriesLocalEpisodes[0], $aTVSeriesFindSeriesLocalEpisodes[UBound($aTVSeriesFindSeriesLocalEpisodes) - 1])

			$aTVSeriesFindSeriesVolumeLabelLocalEpisodes = _ArrayFindAll($aTVSeriesGetSeriesLocalEpisodes, $aTVSeriesSelectedPaths[$iCounterSelectedSeriesPaths - 1], Default, Default, Default, Default, 19)
			$aTVSeriesGetSeriesVolumeLabelLocalEpisodes = _ArrayExtract($aTVSeriesGetSeriesLocalEpisodes, $aTVSeriesFindSeriesVolumeLabelLocalEpisodes[0], $aTVSeriesFindSeriesVolumeLabelLocalEpisodes[UBound($aTVSeriesFindSeriesVolumeLabelLocalEpisodes) - 1])

			;ConsoleWrite($aTVSeriesLocalSeries[$iSeries] & ", " & UBound($aTVSeriesGetSeriesVolumeLabelLocalEpisodes) & ", " & $aTVSeriesGetOneSerie[0][1] & @CRLF)

			For $iThisPart = 0 To UBound($aTVSeriesGetSeriesVolumeLabelLocalEpisodes) - 1

				GUICtrlSetData($LabelTVSeriesLogEpisodesRow0, GUICtrlRead($LabelTVSeriesLogEpisodesRow1))
				GUICtrlSetData($LabelTVSeriesLogEpisodesRow1, GUICtrlRead($LabelTVSeriesLogEpisodesRow2))
				GUICtrlSetData($LabelTVSeriesLogEpisodesRow2, GUICtrlRead($LabelTVSeriesLogEpisodesRow3))

				GUICtrlSetData($LabelTVSeriesLogEpisodesRow3, "Epi no: " & $iThisPart)
				If $iThisPart = UBound($aTVSeriesGetSeriesOnlineEpisodes) Then
					; Problem?
					$iErrorLogCounter += 1
					GUICtrlSetData($LabelTVSeriesLogEpisodesRow3, GUICtrlRead($LabelTVSeriesLogEpisodesRow3) & "Problem?>ExitLoop")
					$sErrorLog = $sErrorLog & "Maybe an Error, number " & $iErrorLogCounter & @CRLF
					$sErrorLog = $sErrorLog & GUICtrlRead($LabelTVSeriesLogSeriesRow2) & @CRLF
					$sErrorLog = $sErrorLog & "Last OK episode: " & GUICtrlRead($LabelTVSeriesLogEpisodesRow2) & @CRLF
					$sErrorLog = $sErrorLog & "Then: " & GUICtrlRead($LabelTVSeriesLogEpisodesRow3) & @CRLF
					MsgBox(0, "Problem", "Maybe a problem with episode. Check the log that will show up at the end of this process...", 2)


					$iTotalDoneEpisodes += 1
					$iThisPart = 0

					TimeNow(100 * ($iTotalDoneSeries / ((UBound($aTVSeriesLocalSeries) - 2) * UBound($aTVSeriesSelectedPaths))))

					$sTextToAddToStatus = "Timer " & $TimeGone & " Time left " & $TimeCalculatedStillLeft
					$sTextToAddToStatus = $sTextToAddToStatus & " series " & $iTotalDoneSeries + 1 & "/" & ((UBound($aTVSeriesLocalSeries) - 2) * UBound($aTVSeriesSelectedPaths))
					$sTextToAddToStatus = $sTextToAddToStatus & " episodes " & $iTotalDoneEpisodes & "/" & $iTVSeriesLocalEpisodesTotalNumberOfEpisodes
					TVSeriesStatusUpdateReplaceLastRow($sTextToAddToStatus)

					GUICtrlSetData($ProgressTVSeries, 100 * (($iTotalDoneSeries + 1) / ((UBound($aTVSeriesLocalSeries) - 2) * UBound($aTVSeriesSelectedPaths))))

					ExitLoop

				EndIf

				If $iThisPart < UBound($aTVSeriesGetSeriesOnlineEpisodes) Then
					GUICtrlSetData($LabelTVSeriesLogEpisodesRow3, GUICtrlRead($LabelTVSeriesLogEpisodesRow3) & "x" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][3])
				Else
					GUICtrlSetData($LabelTVSeriesLogEpisodesRow3, GUICtrlRead($LabelTVSeriesLogEpisodesRow3) & "x?")
				EndIf
				If $iThisPart < UBound($aTVSeriesGetSeriesOnlineEpisodes) And StringCompare($aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][5], "") = 0 Then
					GUICtrlSetData($LabelTVSeriesLogEpisodesRow3, GUICtrlRead($LabelTVSeriesLogEpisodesRow3) & " " & StringLeft($aTVSeriesGetSeriesVolumeLabelLocalEpisodes[$iThisPart][6], 30) & "...")
				ElseIf $iThisPart < UBound($aTVSeriesGetSeriesOnlineEpisodes) Then
					GUICtrlSetData($LabelTVSeriesLogEpisodesRow3, GUICtrlRead($LabelTVSeriesLogEpisodesRow3) & " " & StringLeft($aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][5], 30) & "...")
				EndIf

				Local $sFileNameWithoutExtension
				$sFileNameWithoutExtension = StringTrimRight($aTVSeriesGetSeriesVolumeLabelLocalEpisodes[$iThisPart][0], 4)

				; HERE WE START MAKING THE .NFO FILE

				If FileExists($sFileNameWithoutExtension & ".nfo") And $iReplaceFiles = 0 Then

					; Do nothing
					;ConsoleWrite("NFO Doing nothing! f exist & no rpls" & FileExists($sFileNameWithoutExtension & ".nfo") & " " & GUICtrlRead($CheckboxMovingPicturesReplaceExistingFiles) & @CRLF) ; Used for testing
					GUICtrlSetData($LabelTVSeriesLogEpisodesRow3, GUICtrlRead($LabelTVSeriesLogEpisodesRow3) & ", no nfo ")

				ElseIf GUICtrlRead($CheckboxTVSeriesCreateEpisodesNfo) = 1 Then

					;Do this
					; Open the file for writing (append to the end of a file) and store the handle to a variable.

					Local $hFileOpen = FileOpen($sFileNameWithoutExtension & ".nfo", 2)
					If $hFileOpen = -1 Then
						GUICtrlSetData($LabelTVSeriesLogEpisodesRow3, GUICtrlRead($LabelTVSeriesLogEpisodesRow3) & ", no nfo ")
						;ConsoleWrite($sFileNameWithoutExtension & ".nfo" & @CRLF)
						Local $sThisErrorMessage = "An error occurred whilst writing the episode nfo temporary file." & @CRLF
						$sThisErrorMessage = $sThisErrorMessage & "Error trying to create this file: " & @CRLF
						$sThisErrorMessage = $sThisErrorMessage & $sFileNameWithoutExtension & ".nfo" & @CRLF
						$sThisErrorMessage = $sThisErrorMessage & "Maybe the name is to big och something like that?"
						MsgBox($MB_SYSTEMMODAL, "", $sThisErrorMessage)
						Return False
					EndIf

					GUICtrlSetData($LabelTVSeriesLogEpisodesRow3, GUICtrlRead($LabelTVSeriesLogEpisodesRow3) & ", nfo ")

					; Write data to the file using the handle returned by FileOpen.
					;ConsoleWrite("NFO Doing something!" & @CRLF) ; Used for testing
					Local $sTextToFile = "<?xml version=""1.0"" encoding=""UTF-8""?>" & @CRLF
					$sTextToFile = $sTextToFile & "<!--created on " & @YEAR & "-" & @MON & "-" & @MDAY & " " & @HOUR & ":" & @MIN & ":" & @SEC & " -->" & @CRLF
					$sTextToFile = $sTextToFile & "<episodedetails>" & @CRLF
					$sTextToFile = $sTextToFile & "  <id>" & $aTVSeriesGetSeriesVolumeLabelLocalEpisodes[$iThisPart][3] & "</id>" & @CRLF
					If StringCompare($aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][5], "") = 0 Then
						$sTextToFile = $sTextToFile & "  <title>" & $aTVSeriesGetSeriesVolumeLabelLocalEpisodes[$iThisPart][6] & "</title>" & @CRLF
					Else
						$sTextToFile = $sTextToFile & "  <title>" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][5] & "</title>" & @CRLF
					EndIf
					$sTextToFile = $sTextToFile & "  <showtitle>" & $aTVSeriesGetOneSerie[0][1] & "</showtitle>" & @CRLF
					$sTextToFile = $sTextToFile & "  <plot>" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][7] & "</plot>" & @CRLF
					$sTextToFile = $sTextToFile & "  <season>" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][4] & "</season>" & @CRLF
					$sTextToFile = $sTextToFile & "  <episode>" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][3] & "</episode>" & @CRLF
					$sTextToFile = $sTextToFile & "  <dvd_season>" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][24] & "</dvd_season>" & @CRLF
					$sTextToFile = $sTextToFile & "  <dvd_episode>" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][23] & "</dvd_episode>" & @CRLF
					If StringCompare($aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][8], "") = 0 Then
						$sTextToFile = $sTextToFile & "  <year>" & StringLeft($aTVSeriesGetSeriesVolumeLabelLocalEpisodes[$iThisPart][16], 4) & "</year>" & @CRLF
					Else
						$sTextToFile = $sTextToFile & "  <year>" & StringLeft($aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][8], 4) & "</year>" & @CRLF
					EndIf
					$sTextToFile = $sTextToFile & "  <rating>" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][29] & "</rating>" & @CRLF
					$sTextToFile = $sTextToFile & "  <votes>" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][30] & "</votes>" & @CRLF
					$sTextToFile = $sTextToFile & "  <playcount>" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][53] & "</playcount>" & @CRLF
					$sTextToFile = $sTextToFile & "  <tvdbid>" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][2] & "</tvdbid>" & @CRLF
					$sTextToFile = $sTextToFile & "  <imdbid>" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][26] & "</imdbid>" & @CRLF
					$sTextToFile = $sTextToFile & "  <status>" & $aTVSeriesGetOneSerie[0][4] & "</status>" & @CRLF
					$sTextToFile = $sTextToFile & MultiSingleLine2MultiXmlRows("  <genre>", $aTVSeriesGetOneSerie[0][5], "</genre>")
					$sTextToFile = $sTextToFile & "  <studio>" & $aTVSeriesGetOneSerie[0][34] & "</studio>" & @CRLF
					For $iThisActor = 0 To UBound($aTVSeriesGetAllActorsForOneSeries) - 1
						$sTextToFile = $sTextToFile & "  <actor>" & @CRLF & "    <name>" & $aTVSeriesGetAllActorsForOneSeries[$iThisActor][3] & "</name>" & @CRLF
						$sTextToFile = $sTextToFile & "    <role>" & $aTVSeriesGetAllActorsForOneSeries[$iThisActor][4] & "</role>" & @CRLF & "  </actor>" & @CRLF
					Next
					$sTextToFile = $sTextToFile & "</episodedetails>"
					FileWrite($hFileOpen, $sTextToFile)

					; Close the handle returned by FileOpen.
					FileClose($hFileOpen)


					If GUICtrlRead($CheckboxTVSeriesSameTimestampAsMedia) = 1 Then
						FileSetTime($sFileNameWithoutExtension & ".nfo", FileGetTime($aTVSeriesGetSeriesVolumeLabelLocalEpisodes[$iThisPart][0], $FT_MODIFIED, $FT_STRING), $FT_MODIFIED)
					EndIf

				EndIf

				If FileExists($sFileNameWithoutExtension & ".jpg") And $iReplaceFiles = 0 Then
					GUICtrlSetData($LabelTVSeriesLogEpisodesRow3, GUICtrlRead($LabelTVSeriesLogEpisodesRow3) & ", no banner ")
					; Do nothing
					;ConsoleWrite("NFO Doing nothing! f exist & no rpls" & FileExists($sFileNameWithoutExtension & ".nfo") & " " & GUICtrlRead($CheckboxMovingPicturesReplaceExistingFiles) & @CRLF) ; Used for testing

				ElseIf GUICtrlRead($CheckboxTVSeriesCopyEpisodePics) = 1 Then
					GUICtrlSetData($LabelTVSeriesLogEpisodesRow3, GUICtrlRead($LabelTVSeriesLogEpisodesRow3) & ", banner ")

					Local $sFileToCopy1, $sFileToCopy2, $sFileToCreate, $sFileCopyINfo, $aFileFolderArray, $sFileFolder

					$aFileFolderArray = StringSplit((StringReplace($aTVSeriesGetOneSerie[0][7], "/", "\")), "\")

					$sFileFolder = $sTVSeriesProgramDataFolder & "\Thumbs\MPTVSeriesBanners\" & $aFileFolderArray[1] & "\Episodes\"

					;ConsoleWrite($aTVSeriesGetSeriesVolumeLabelLocalEpisodes[$iThisPart][3] & " " & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][4] & "x" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][3] & " " & $aTVSeriesGetOneSerie[0][1] & @CRLF)
					;This is for normal series
					$sFileToCopy1 = $sFileFolder & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][4] & "x" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][3] & ".jpg"
					;This is for custom series and custom episodes
					$sFileToCopy2 = $sFileFolder & "custom-" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][4] & "x" & $aTVSeriesGetSeriesOnlineEpisodes[$iThisPart][3] & ".jpg"

					$sFileToCreate = $sFileNameWithoutExtension & ".jpg"

					;ConsoleWrite($sFileCopyINfo & " = " & $sFileToCopy1 & " > " & $sFileToCreate & @CRLF)
					$sFileCopyINfo = FileCopy($sFileToCopy1, $sFileToCreate, $iReplaceFiles)
					;ConsoleWrite($sFileCopyINfo & " = " & $sFileToCopy2 & " > " & $sFileToCreate & @CRLF)
					$sFileCopyINfo = FileCopy($sFileToCopy2, $sFileToCreate, $iReplaceFiles)

					If GUICtrlRead($CheckboxTVSeriesSameTimestampAsMedia) = 1 Then
						FileSetTime($sFileNameWithoutExtension & ".jpg", FileGetTime($aTVSeriesGetSeriesVolumeLabelLocalEpisodes[$iThisPart][0], $FT_MODIFIED, $FT_STRING), $FT_MODIFIED)
					EndIf
				EndIf

				If $iCancel = 1 Then
					; YES $IDYES (6)
					; NO $IDNO (7)
					TVSeriesStatusUpdateNewRow("Do you want to cancel?")
					$CancelMessageBox = MsgBox(4, "Cancel?", "Do you want to cancel?")
					;ConsoleWrite("box = " & $CancelMessageBox)
					If $CancelMessageBox = 6 Then
						_SQLite_Close($hDskDb)         ; DB is a regular file that could be reopened later
						_SQLite_Shutdown()

						TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = Yes.")
						$aMovingPicturesLocalMedia = 0

						;Enable Delete radio button again...
						GUICtrlSetState($RadioTVSeriesDeleteMetadata, $GUI_ENABLE)

						GUICtrlSetState($RadioTVSeriesCreateMetadata, $GUI_ENABLE)

						GUICtrlSetState($LabelTVSeriesWorking, $GUI_HIDE)
						GUICtrlSetState($TabSheet1, $GUI_ENABLE)

						$aTVSeriesAllOnlineEpisodes = 0
						$aTVSeriesAllLocalEpisodes = 0
						$aTVSeriesAllActors = 0
						$aTVSeriesAllOnline_series = 0
						$aTVSeriesAllSeasons = 0
						$aTVSeriesAllFanart = 0

						$iCancel = 0
						$TimerSet = 0
						;ExitLoop
						Return

					Else
						TVSeriesStatusUpdateMoreOnLastRow(" --> Answer = No.")
						$iCancel = 0
					EndIf
				EndIf

				$iTotalDoneEpisodes += 1

				TimeNow(100 * ($iTotalDoneSeries / ((UBound($aTVSeriesLocalSeries) - 2) * UBound($aTVSeriesSelectedPaths))))

				$sTextToAddToStatus = "Timer " & $TimeGone & " Time left " & $TimeCalculatedStillLeft
				$sTextToAddToStatus = $sTextToAddToStatus & " series " & $iTotalDoneSeries + 1 & "/" & ((UBound($aTVSeriesLocalSeries) - 2) * UBound($aTVSeriesSelectedPaths))
				$sTextToAddToStatus = $sTextToAddToStatus & " episodes " & $iTotalDoneEpisodes & "/" & $iTVSeriesLocalEpisodesTotalNumberOfEpisodes
				TVSeriesStatusUpdateReplaceLastRow($sTextToAddToStatus)

				GUICtrlSetData($ProgressTVSeries, 100 * (($iTotalDoneSeries + 1) / ((UBound($aTVSeriesLocalSeries) - 2) * UBound($aTVSeriesSelectedPaths))))

			Next

			$iTotalDoneSeries += 1

		Next


	Next ;For $iCounterSelectedSeriesPaths = 1 To UBound($aTVSeriesSelectedPaths)

	; Remove all info in array to save memory
	$aTVSeriesAllOnlineEpisodes = 0
	$aTVSeriesAllLocalEpisodes = 0
	$aTVSeriesAllActors = 0
	$aTVSeriesAllOnline_series = 0
	$aTVSeriesAllSeasons = 0
	$aTVSeriesAllFanart = 0

	$TimerSet = 0

	_SQLite_Close($hDskDb)         ; DB is a regular file that could be reopened later
	_SQLite_Shutdown()

	TVSeriesStatusUpdateNewRow("Done creating files!")
	Run("notepad.exe")
	WinWaitActive("[CLASS:Notepad]", "", 10)
	Send("Error log. Maybe problems or maybe glitch in software (if empty, be happy ;):" & @CRLF & $sErrorLog)

	;Enable Delete radio buttons again...
	GUICtrlSetState($RadioTVSeriesDeleteMetadata, $GUI_ENABLE)

	GUICtrlSetState($ButtonTVSeriesCreateMetadata, $GUI_ENABLE)

EndFunc   ;==>TVSeriesCreateXMLandCopyPictures


Func MovingPicturesCreateXMLandCopyPictures()

	;Disable Delete radio button while we do this, so the user don't get any ideas ;-)...
	GUICtrlSetState($RadioMovingPicturesDeleteMetadata, $GUI_DISABLE)

	;Disable Create button to avoid dubble clicking...
	GUICtrlSetState($ButtonMovingPicturesCreateMetadata, $GUI_DISABLE)

	;Set progress = 0
	GUICtrlSetData($ProgressMovingPictures, 0)

	;Get checked folders
	MovingPicturesGetCheckedFolderCheckboxes()
	;$aMovingPicturesLocalMediaInfoMovies have all movies from checked folders

	MovingPicturesStatusUpdateNewRow("Create: Starting SQLite... ")
	_SQLite_Startup($sSQLiteDll_Filename, False, 1)
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "SQLite Error", "SQLite3.dll Can't be Loaded!" & $sSQLiteDll_Filename)
		Exit -1
	EndIf
;~ 	ConsoleWrite("_SQLite_LibVersion=" & _SQLite_LibVersion() & @CRLF) ; For testing in console

	MovingPicturesStatusUpdateMoreOnLastRow("Opening database... ")
	Local $hDskDb = _SQLite_Open($sMovingPicturesDatabase)         ; Open a permanent disk database
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "SQLite Error", "Can't open or create a permanent Database!")
		Exit -1
	EndIf

	Global $aMovingPicturesLocalMediaInfoMovies[UBound($aMovingPicturesSelectedPathIDs)][2]
	Local $iMovieProcessCounter = 1

	;Run this once for every checked folder
	MovingPicturesStatusUpdateMoreOnLastRow("Processing folder")
	For $iCounterSelectedMoviesPaths = 1 To UBound($aMovingPicturesSelectedPathIDs)
		MovingPicturesStatusUpdateReplaceLastRow("On folder: " & $iCounterSelectedMoviesPaths)
		;Get all movies from one folder
		$iReturnValue = _SQLite_GetTable2d(-1, "SELECT * FROM local_media WHERE importpath = " & $aMovingPicturesSelectedPathIDs[$iCounterSelectedMoviesPaths - 1] & ";", $aMovingPicturesLocalMedia, $iMovingPicturesLocalMediaRows, $iMovingPicturesLocalMediaColumns)
		;If all went well $aMovingPicturesLocalMedia have all movies in folder, continue
		If $iReturnValue = $SQLITE_OK Then

;~ Content of $aMovingPicturesLocalMedia
;~
;~ Important columns from local_media
;~ XML		TABLE		COLUMN	NAME		NOTE
;~ 			local_media	0		id			ID in table local_media, via local_media__movie_info we get movie_info ID
;~ 			local_media	2		fullpath	Full path and file name of movie
;~ <part>	local_media	6		part		What part, say CD1 of 2 CDs

;~ 			_ArrayDisplay($aMovingPicturesLocalMedia) ; For testing
;~
;~ 			OK, let's go through all the files in the folder
;~ 			The rows and columns are found like this $aMovingPicturesLocalMedia[rows][columns]
;~ 			NOTE: First row (number 0) in $aMovingPicturesLocalMedia contains headersl, so we start with 1
;~			ConsoleWrite("UBound($aMovingPicturesLocalMedia): " & UBound($aMovingPicturesLocalMedia) & @CRLF); For testing

;~ 			To get info from local_media__movie_info
			Local $aSQLresult, $iSQLresultRows, $iSQLresultColumns
;~ 			To get info from movie_info
			Local $aMovingPicturesMovieInfo, $iMovingPicturesMovieInfoRows, $iMovingPicturesMovieInfoColumns
			Local $aMovingPicturesMovieWatchedInfo, $iMovingPicturesMovieWatchedInfoRows, $iMovingPicturesMovieWatchedInfoColumns

			Local $iReplaceFiles = 0
			If GUICtrlRead($CheckboxMovingPicturesReplaceExistingFiles) = 1 Then
				$iReplaceFiles = 1
			EndIf

			For $y = 1 To UBound($aMovingPicturesLocalMedia) - 1
				;Sleep(1000)
				;ConsoleWrite(" $iCancel = " & $iCancel)
				If $iCancel = 1 Then
					; YES $IDYES (6)
					; NO $IDNO (7)
					MovingPicturesStatusUpdateNewRow("Do you want to cancel?")
					$CancelMessageBox = MsgBox(4, "Cancel?", "Do you want to cancel?")
					;ConsoleWrite("box = " & $CancelMessageBox)
					If $CancelMessageBox = 6 Then
						_SQLite_Close($hDskDb)         ; DB is a regular file that could be reopened later
						_SQLite_Shutdown()

						MovingPicturesStatusUpdateMoreOnLastRow(" --> Answer = Yes. Stopped at movie " & $y & " of " & UBound($aMovingPicturesLocalMedia) - 1 & " in folder: " & $iCounterSelectedMoviesPaths)
						$aMovingPicturesLocalMedia = 0

						;Enable Delete radio button again...
						GUICtrlSetState($RadioMovingPicturesDeleteMetadata, $GUI_ENABLE)

						GUICtrlSetState($ButtonMovingPicturesCreateMetadata, $GUI_ENABLE)

						$iCancel = 0
						$TimerSet = 0
						;ExitLoop
						Return
					Else
						MovingPicturesStatusUpdateMoreOnLastRow(" --> Answer = No.")
						$iCancel = 0
					EndIf
				EndIf

				;ConsoleWrite("local_media_id: " & $aMovingPicturesLocalMedia[$y][0] & " and "); Used for testing

;~ 				Get movie_info ID from local_media__movie_info table
;~ 				The ID is in the third column = $aSQLresult[2]
				$iReturnValue = _SQLite_GetTable(-1, "SELECT movie_info_id FROM local_media__movie_info WHERE local_media_id = " & $aMovingPicturesLocalMedia[$y][0] & ";", $aSQLresult, $iSQLresultRows, $iSQLresultColumns)
				;ConsoleWrite("movie_info_id: " & $aSQLresult[2] & " and ") ; Used for testing

;~ 				Now we get info from movie_info
;~ 				First row (number 0) have the table headers names, next row (1) we have the data
				$iReturnValue = _SQLite_GetTable2d(-1, "SELECT * FROM movie_info WHERE id = " & $aSQLresult[2] & ";", $aMovingPicturesMovieInfo, $iMovingPicturesMovieInfoRows, $iMovingPicturesMovieInfoColumns)
				;ConsoleWrite("id: " & $aMovingPicturesMovieInfo[1][0] & " & movie " & $aMovingPicturesMovieInfo[1][1] & " & columns " & $iMovingPicturesMovieInfoColumns); Used for testing


;~ 				And the last part is to get watched count from the user_movie_settings table
;~				In user_movie_settings, id = movie_info_id and column 3 = watched
				$iReturnValue = _SQLite_GetTable(-1, "SELECT watched FROM user_movie_settings WHERE id = " & $aSQLresult[2] & ";", $aMovingPicturesMovieWatchedInfo, $iMovingPicturesMovieWatchedInfoRows, $iMovingPicturesMovieWatchedInfoColumns)
				;ConsoleWrite(" & watched = " & $aMovingPicturesMovieWatchedInfo[2] & @CRLF); Used for testing
				;_ArrayDisplay($aMovingPicturesMovieWatchedInfo) ; For testing

				Local $sFileNameWithoutExtension
				$sFileNameWithoutExtension = StringTrimRight($aMovingPicturesLocalMedia[$y][2], 4)

				; HERE WE START MAKING THE .NFO FILE

				If FileExists($sFileNameWithoutExtension & ".nfo") And $iReplaceFiles = 0 Then

					; Do nothing
					;ConsoleWrite("NFO Doing nothing! f exist & no rpls" & FileExists($sFileNameWithoutExtension & ".nfo") & " " & GUICtrlRead($CheckboxMovingPicturesReplaceExistingFiles) & @CRLF) ; Used for testing

				ElseIf GUICtrlRead($CheckboxMovingPicturesCreateNfo) = 1 Then

					;Do this
					; Open the file for writing (append to the end of a file) and store the handle to a variable.
					Local $hFileOpen = FileOpen($sFileNameWithoutExtension & ".nfo", 2)
					If $hFileOpen = -1 Then
						MsgBox($MB_SYSTEMMODAL, "", "An error occurred whilst writing the Moving Pictures nfo temporary file.")
						Return False
					EndIf

;~ Info from
;~ https://www.team-mediaportal.com/wiki/display/MediaPortal2/NfoMovieMetadataExtractor
;~ https://www.team-mediaportal.com/wiki/display/glossary/NFO+Node+-+Lexical+support
;~ Test .nfo from Media-Buddy
;~ XML				TABLE         COLUMN                EXAMPLE DATA
;~ 					local_media   0  id                 1799
;~ 					movie_info    0  id                 1584 (via 1799 & local_media__movie_info table)
;~ *<title>         movie_info    1  title              2 Fast 2 Furious
;~ *<sorttitle>     movie_info    3  sortby             2 fast 2 furious
;~ *<originaltitle>
;~ *<set>           movie_info    28 collections        (multiple XML (or |?)) The Fast and the Furious
;~ *<part>          local_media    6 part               1
;~ *<rating>        movie_info    13 score              5.2
;~ *<votes>         movie_info    14 popularity         243
;~ *<year>          movie_info    07 year               2003
;~ *<plot>          movie_info    12 summary            Former cop Brian O'Conner...
;~ *<tagline>       movie_info    11 tagline            2Cool
;~ *<runtime>       movie_info    16 runtime            107
;~ *<mpaa>          movie_info    09 certification      PG-13
;~ <certification>  movie_info    09 certification      PG-13
;~ *<language>      movie_info    10 language           (multiple XML or |?) English
;~ *<id>            movie_info    17 imdb_id            tt0322259
;~ *<imdb>          movie_info    17 imdb_id            tt0322259
;~ *<director>      movie_info    04 directors          (multiple XML or |?) |Benny Chan|Hanna Saw|
;~ *<studios>       movie_info    26 studios            (multiple XML) |DreamWorks Animation|Bullwinkle Studios|
;~ *<playcount>     user_movie_settings (id = movie_info_id) column 3 - watched
;~ *<genre>         movie_info    08 genres             (multiple XML) |Action|Crime|Thriller|
;~ *<actor><name>   movie_info    6  actors             (multiple XML) |Paul Walker|Tyrese Gibson|

;~ PICTURE FILES
;~ name-thumb.jpg  	movie_info    21 coverthumbfullpath full path\{2.Fast.2.Furious-2003} [-1299615737].jpg
;~ name-poster.jpg 	movie_info    20 coverfullpath      full path\{2.Fast.2.Furious-2003} [-1299615737].jpg
;~ name-fanart.jpg 	movie_info    22 backdropfullpath   full path\{2.Fast.2.Furious-2003} [949576310].jpg

					; Write data to the file using the handle returned by FileOpen.
					;ConsoleWrite("NFO Doing something!" & @CRLF) ; Used for testing
					Local $sTextToFile = "<?xml version=""1.0"" encoding=""UTF-8""?>" & @CRLF
					$sTextToFile = $sTextToFile & "<!--created on " & @YEAR & "-" & @MON & "-" & @MDAY & " " & @HOUR & ":" & @MIN & ":" & @SEC & " -->" & @CRLF
					$sTextToFile = $sTextToFile & "<movie>" & @CRLF
					$sTextToFile = $sTextToFile & "  <title>" & $aMovingPicturesMovieInfo[1][1] & "</title>" & @CRLF
					$sTextToFile = $sTextToFile & "  <set>" & $aMovingPicturesMovieInfo[1][28] & "</set>" & @CRLF
					$sTextToFile = $sTextToFile & "  <part>" & $aMovingPicturesLocalMedia[$y][6] & "</part>" & @CRLF
					$sTextToFile = $sTextToFile & "  <rating>" & $aMovingPicturesMovieInfo[1][13] & "</rating>" & @CRLF
					$sTextToFile = $sTextToFile & "  <votes>" & $aMovingPicturesMovieInfo[1][14] & "</votes>" & @CRLF
					$sTextToFile = $sTextToFile & "  <year>" & $aMovingPicturesMovieInfo[1][7] & "</year>" & @CRLF
					$sTextToFile = $sTextToFile & "  <plot>" & $aMovingPicturesMovieInfo[1][12] & "</plot>" & @CRLF
					$sTextToFile = $sTextToFile & "  <tagline>" & $aMovingPicturesMovieInfo[1][11] & "</tagline>" & @CRLF
					$sTextToFile = $sTextToFile & "  <runtime>" & $aMovingPicturesMovieInfo[1][16] & "</runtime>" & @CRLF
					$sTextToFile = $sTextToFile & "  <mpaa>" & $aMovingPicturesMovieInfo[1][9] & "</mpaa>" & @CRLF
					$sTextToFile = $sTextToFile & MultiSingleLine2MultiXmlRows("  <language>", $aMovingPicturesMovieInfo[1][10], "</language>")
					$sTextToFile = $sTextToFile & "  <id>" & $aMovingPicturesMovieInfo[1][17] & "</id>" & @CRLF
					$sTextToFile = $sTextToFile & "  <imdb>" & $aMovingPicturesMovieInfo[1][17] & "</imdb>" & @CRLF
					$sTextToFile = $sTextToFile & "  <director>" & $aMovingPicturesMovieInfo[1][4] & "</director>" & @CRLF
					$sTextToFile = $sTextToFile & MultiSingleLine2MultiXmlRows("  <studios>", $aMovingPicturesMovieInfo[1][26], "</studios>")
					$sTextToFile = $sTextToFile & "  <playcount>" & $aMovingPicturesMovieWatchedInfo[2] & "</playcount>" & @CRLF
					$sTextToFile = $sTextToFile & MultiSingleLine2MultiXmlRows("  <genre>", $aMovingPicturesMovieInfo[1][8], "</genre>")
					$sTextToFile = $sTextToFile & MultiSingleLine2MultiXmlRows("  <actor>" & @CRLF & "    <name>", $aMovingPicturesMovieInfo[1][6], "</name>" & @CRLF & "  </actor>")
					$sTextToFile = $sTextToFile & "</movie>"
					FileWrite($hFileOpen, $sTextToFile)

					; Close the handle returned by FileOpen.
					FileClose($hFileOpen)

					If GUICtrlRead($CheckboxMovingPicturesSameTimestampAsMedia) = 1 Then
						FileSetTime($sFileNameWithoutExtension & ".nfo", FileGetTime($aMovingPicturesLocalMedia[$y][2], $FT_MODIFIED, $FT_STRING), $FT_MODIFIED)
					EndIf

				Else
					; Do nothing
					;ConsoleWrite("NFO Doing nothing! ChkBx .nfo" & GUICtrlRead($CheckboxMovingPicturesCreateNfo) & @CRLF) ; Used for testing
				EndIf

;~ PICTURE FILES
;~ name-thumb.jpg  	movie_info    21 coverthumbfullpath full path\{2.Fast.2.Furious-2003} [-1299615737].jpg
;~ name-poster.jpg 	movie_info    20 coverfullpath      full path\{2.Fast.2.Furious-2003} [-1299615737].jpg
;~ name-fanart.jpg 	movie_info    22 backdropfullpath   full path\{2.Fast.2.Furious-2003} [949576310].jpg
				; FANART
				; HERE WE START COPYING THE -fanart.jpg FILE
				If FileExists($sFileNameWithoutExtension & "-fanart.jpg") And $iReplaceFiles = 0 Then
					; Do nothing
					;ConsoleWrite("Fanart Doing nothing! f exist & no rpls" & FileExists($sFileNameWithoutExtension & "-fanart.jpg") & " " & $iReplaceFiles & @CRLF) ; Used for testing
				ElseIf GUICtrlRead($CheckboxMovingPicturesCopyFanart) = 1 Then
					;Do this
					FileCopy($aMovingPicturesMovieInfo[1][22], $sFileNameWithoutExtension & "-fanart.jpg", $iReplaceFiles)
					If GUICtrlRead($CheckboxMovingPicturesSameTimestampAsMedia) = 1 Then
						FileSetTime($sFileNameWithoutExtension & "-fanart.jpg", FileGetTime($aMovingPicturesLocalMedia[$y][2], $FT_MODIFIED, $FT_STRING), $FT_MODIFIED)
					EndIf
				Else
					; Do nothing
					;ConsoleWrite("Fanart Doing nothing! ChkBx" & GUICtrlRead($CheckboxMovingPicturesCopyFanart) & @CRLF) ; Used for testing
				EndIf
				; POSTER
				; HERE WE START COPYING THE -poster.jpg FILE
				If FileExists($sFileNameWithoutExtension & "-poster.jpg") And $iReplaceFiles = 0 Then
					; Do nothing
					;ConsoleWrite("Poster Doing nothing! f exist & no rpls" & FileExists($sFileNameWithoutExtension & "-poster.jpg") & " " & $iReplaceFiles & @CRLF) ; Used for testing
				ElseIf GUICtrlRead($CheckboxMovingPicturesCopyCover) = 1 Then
					;Do this
					FileCopy($aMovingPicturesMovieInfo[1][20], $sFileNameWithoutExtension & "-poster.jpg", $iReplaceFiles)
					If GUICtrlRead($CheckboxMovingPicturesSameTimestampAsMedia) = 1 Then
						FileSetTime($sFileNameWithoutExtension & "-poster.jpg", FileGetTime($aMovingPicturesLocalMedia[$y][2], $FT_MODIFIED, $FT_STRING), $FT_MODIFIED)
					EndIf
				Else
					; Do nothing
					;ConsoleWrite("Poster Doing nothing! ChkBx" & GUICtrlRead($CheckboxMovingPicturesCopyCover) & @CRLF) ; Used for testing
				EndIf
				; THUMB
				; HERE WE START COPYING THE -thumb.jpg FILE
				If FileExists($sFileNameWithoutExtension & "-thumb.jpg") And $iReplaceFiles = 0 Then
					; Do nothing
					;ConsoleWrite("Thumb Doing nothing! f exist & no rpls" & FileExists($sFileNameWithoutExtension & "-thumb.jpg") & " " & $iReplaceFiles & @CRLF) ; Used for testing
				ElseIf GUICtrlRead($CheckboxMovingPicturesCopyThumb) = 1 Then
					;Do this
					FileCopy($aMovingPicturesMovieInfo[1][21], $sFileNameWithoutExtension & "-thumb.jpg", $iReplaceFiles)
					If GUICtrlRead($CheckboxMovingPicturesSameTimestampAsMedia) = 1 Then
						FileSetTime($sFileNameWithoutExtension & "-thumb.jpg", FileGetTime($aMovingPicturesLocalMedia[$y][2], $FT_MODIFIED, $FT_STRING), $FT_MODIFIED)
					EndIf
				Else
					; Do nothing
					;ConsoleWrite("Thumb Doing nothing! ChkBx" & GUICtrlRead($CheckboxMovingPicturesCopyThumb) & @CRLF) ; Used for testing
				EndIf

				; Write info to status objects
				;ConsoleWrite(".NFO path: " & $sFileNameWithoutExtension & ".nfo" & " % = " & 100*($iMovieProcessCounter/$iMovingPicturesLocalMediaTotalNumberOfMovies) & @CRLF); Used for testing
				$iMovieProcessCounter += 1
				GUICtrlSetData($ProgressMovingPictures, 100 * ($iMovieProcessCounter / $iMovingPicturesLocalMediaTotalNumberOfMovies))
				TimeNow(100 * ($iMovieProcessCounter / $iMovingPicturesLocalMediaTotalNumberOfMovies))
				MovingPicturesStatusUpdateReplaceLastRow("Timer " & $TimeGone & " Time left " & $TimeCalculatedStillLeft & " Processing folder: " & $iCounterSelectedMoviesPaths & " > " & StringFormat("%#.2f", 100 * (($iMovieProcessCounter - 1) / $iMovingPicturesLocalMediaTotalNumberOfMovies), 2) & "% media " & $iMovieProcessCounter - 1 & "/" & $iMovingPicturesLocalMediaTotalNumberOfMovies)

			Next

		Else
			MsgBox($MB_SYSTEMMODAL, "SQLite Error: " & $iReturnValue, _SQLite_ErrMsg())
		EndIf
	Next

	; Remove all info in array to save memory
	$aMovingPicturesLocalMedia = 0
	$TimerSet = 0

	_SQLite_Close($hDskDb)         ; DB is a regular file that could be reopened later
	_SQLite_Shutdown()

	MovingPicturesStatusUpdateNewRow("Done creating files!")

	;Enable Delete radio button again...
	GUICtrlSetState($RadioMovingPicturesDeleteMetadata, $GUI_ENABLE)

	GUICtrlSetState($ButtonMovingPicturesCreateMetadata, $GUI_ENABLE)

EndFunc   ;==>MovingPicturesCreateXMLandCopyPictures

Func MovingPicturesDeleteFiles()

	;Disable Create radio button while we do this, so the user don't get any ideas ;-)...
	GUICtrlSetState($RadioMovingPicturesCreateMetadata, $GUI_DISABLE)

	GUICtrlSetState($ButtonMovingPicturesDeleteMetadata, $GUI_DISABLE)

	;Set progress = 0
	GUICtrlSetData($ProgressMovingPictures, 0)

	;Get checked folders
	MovingPicturesGetCheckedFolderCheckboxes()
	;$aMovingPicturesLocalMediaInfoMovies have all movies from checked folders

	MovingPicturesStatusUpdateNewRow("Create: Starting SQLite... ")
	_SQLite_Startup($sSQLiteDll_Filename, False, 1)
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "SQLite Error", "SQLite3.dll Can't be Loaded!" & $sSQLiteDll_Filename)
		Exit -1
	EndIf
;~ 	ConsoleWrite("_SQLite_LibVersion=" & _SQLite_LibVersion() & @CRLF) ; For testing in console

	MovingPicturesStatusUpdateMoreOnLastRow("Opening database... ")
	Local $hDskDb = _SQLite_Open($sMovingPicturesDatabase)         ; Open a permanent disk database
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "SQLite Error", "Can't open or create a permanent Database!")
		Exit -1
	EndIf

	Local $DeleteInfo = "Do you really want to delete all selected files in these folders?" & @CRLF
	For $iCounterSelectedMoviesPaths = 1 To UBound($aMovingPicturesSelectedPathIDs)
		$DeleteInfo = $DeleteInfo & $aMovingPicturesSelectedPaths[$iCounterSelectedMoviesPaths - 1] & @CRLF
	Next

	; YES $IDYES (6)
	; NO $IDNO (7)
	$DeleteMessageBox = MsgBox(4, "Delete files?", $DeleteInfo)
	If $DeleteMessageBox = 7 Then
		MovingPicturesStatusUpdateNewRow("Deletion aborted by user!")
		;Enable Create radio button again...
		GUICtrlSetState($RadioMovingPicturesCreateMetadata, $GUI_ENABLE)
		Return
	EndIf

	Local $iMovieProcessCounter = 1

	;Run this once for every checked folder
	MovingPicturesStatusUpdateMoreOnLastRow("Deleting in folder")
	For $iCounterSelectedMoviesPaths = 1 To UBound($aMovingPicturesSelectedPathIDs)

		If $iCancel = 1 Then
			; YES $IDYES (6)
			; NO $IDNO (7)
			MovingPicturesStatusUpdateNewRow("Do you want to cancel?")
			$CancelMessageBox = MsgBox(4, "Cancel?", "Do you want to cancel?")
			;ConsoleWrite("box = " & $CancelMessageBox)
			If $CancelMessageBox = 6 Then
				_SQLite_Close($hDskDb)                 ; DB is a regular file that could be reopened later
				_SQLite_Shutdown()

				MovingPicturesStatusUpdateMoreOnLastRow(" --> Answer = Yes. Stopped at path " & $iCounterSelectedMoviesPaths & " of " & UBound($aMovingPicturesSelectedPathIDs) - 1 & " in folder: " & $iCounterSelectedMoviesPaths)
				$aMovingPicturesLocalMedia = 0

				;Enable Delete radio button again...
				GUICtrlSetState($RadioMovingPicturesCreateMetadata, $GUI_ENABLE)

				GUICtrlSetState($ButtonMovingPicturesDeleteMetadata, $GUI_ENABLE)

				$iCancel = 0
				$TimerSet = 0
				;ExitLoop
				Return
			Else
				MovingPicturesStatusUpdateMoreOnLastRow(" --> Answer = No.")
				$iCancel = 0
			EndIf
		EndIf

		MovingPicturesStatusUpdateNewRow("Deleting in folder " & $iCounterSelectedMoviesPaths)
		;Show folder
		;ConsoleWrite($aMovingPicturesSelectedPaths[$iCounterSelectedMoviesPaths - 1] & @CRLF)

		If GUICtrlRead($CheckboxMovingPicturesCreateNfo) = 1 Then
			GUICtrlSetData($ProgressMovingPictures, 100 * (((1 / 8) * $iMovieProcessCounter)) / UBound($aMovingPicturesSelectedPathIDs))
			MovingPicturesStatusUpdateMoreOnLastRow(" *.nfo")
			FileDelete($aMovingPicturesSelectedPaths[$iCounterSelectedMoviesPaths - 1] & "\*.nfo")
		EndIf
		If GUICtrlRead($CheckboxMovingPicturesCopyCover) = 1 Then
			GUICtrlSetData($ProgressMovingPictures, 100 * (((2 / 8) * $iMovieProcessCounter)) / UBound($aMovingPicturesSelectedPathIDs))
			MovingPicturesStatusUpdateMoreOnLastRow(" *-poster.jpg")
			FileDelete($aMovingPicturesSelectedPaths[$iCounterSelectedMoviesPaths - 1] & "\*-poster.jpg")
		EndIf
		If GUICtrlRead($CheckboxMovingPicturesCopyFanart) = 1 Then
			GUICtrlSetData($ProgressMovingPictures, 100 * (((3 / 8) * $iMovieProcessCounter)) / UBound($aMovingPicturesSelectedPathIDs))
			MovingPicturesStatusUpdateMoreOnLastRow(" *-fanart.jpg")
			FileDelete($aMovingPicturesSelectedPaths[$iCounterSelectedMoviesPaths - 1] & "\*-fanart.jpg")
		EndIf
		If GUICtrlRead($CheckboxMovingPicturesCopyThumb) = 1 Then
			GUICtrlSetData($ProgressMovingPictures, 100 * (((4 / 8) * $iMovieProcessCounter)) / UBound($aMovingPicturesSelectedPathIDs))
			MovingPicturesStatusUpdateMoreOnLastRow(" *-thumb.jpg")
			FileDelete($aMovingPicturesSelectedPaths[$iCounterSelectedMoviesPaths - 1] & "\*-thumb.jpg")
		EndIf
		If GUICtrlRead($CheckboxMovingPicturesReplaceExistingFiles) = 1 Then
			GUICtrlSetData($ProgressMovingPictures, 100 * (((5 / 8) * $iMovieProcessCounter)) / UBound($aMovingPicturesSelectedPathIDs))
			MovingPicturesStatusUpdateMoreOnLastRow(" *.jpg")
			FileDelete($aMovingPicturesSelectedPaths[$iCounterSelectedMoviesPaths - 1] & "\*.jpg")
			GUICtrlSetData($ProgressMovingPictures, 100 * (((6 / 8) * $iMovieProcessCounter)) / UBound($aMovingPicturesSelectedPathIDs))
			MovingPicturesStatusUpdateMoreOnLastRow(" *.xml")
			FileDelete($aMovingPicturesSelectedPaths[$iCounterSelectedMoviesPaths - 1] & "\*.xml")
			GUICtrlSetData($ProgressMovingPictures, 100 * (((7 / 8) * $iMovieProcessCounter)) / UBound($aMovingPicturesSelectedPathIDs))
			MovingPicturesStatusUpdateMoreOnLastRow(" *.txt")
			FileDelete($aMovingPicturesSelectedPaths[$iCounterSelectedMoviesPaths - 1] & "\*.txt")
		EndIf

		; Write info to status objects
		$iMovieProcessCounter += 1
		GUICtrlSetData($ProgressMovingPictures, 100 * ($iMovieProcessCounter / UBound($aMovingPicturesSelectedPathIDs)))

		;MovingPicturesStatusUpdateReplaceLastRow("Deleting in folder: " & $iCounterSelectedMoviesPaths & " " & 100 * (($iMovieProcessCounter - 1) / UBound($aMovingPicturesSelectedPathIDs)) & "%")

	Next

	; Remove all info in array to save memory
	$aMovingPicturesLocalMedia = 0

	_SQLite_Close($hDskDb)         ; DB is a regular file that could be reopened later
	_SQLite_Shutdown()

	MovingPicturesStatusUpdateNewRow("Done deleting files!")
	;Enable Create radio button again...
	GUICtrlSetState($RadioMovingPicturesCreateMetadata, $GUI_ENABLE)

	GUICtrlSetState($ButtonMovingPicturesDeleteMetadata, $GUI_ENABLE)

EndFunc   ;==>MovingPicturesDeleteFiles

Func MultiSingleLine2MultiXmlRows($sStartXml, $sMultiSingelLine, $sStopXml)

	Local $sMultiXmlRows = $sStartXml & $sMultiSingelLine & $sStopXml & @CRLF
	Local $aMultiXmlRows = StringSplit($sMultiSingelLine, "|")
	For $x = 2 To UBound($aMultiXmlRows) - 2
		;ConsoleWrite($sMultiXmlRows & @CRLF)
		If $x = 2 Then
			$sMultiXmlRows = ""
		EndIf
		$sMultiXmlRows = $sMultiXmlRows & $sStartXml & $aMultiXmlRows[$x] & $sStopXml & @CRLF
	Next
	;_ArrayDisplay($aMultiXmlRows)

	Return $sMultiXmlRows

EndFunc   ;==>MultiSingleLine2MultiXmlRows

Func StartMainGUI()
	While 1
		$nMsg = GUIGetMsg()
		Switch $nMsg
			Case $GUI_EVENT_CLOSE
				Exit
			Case $MenuExit
				Exit
			Case $MenuAbout
				MsgBox(0, "About", $sAbout)
			Case $MenuHelpInfo
				ShowHelp()
				;Stuff for Moving Pictures tab
				;*****************************
			Case $ButtonMovingPicturesPathCheckAll
				_GUICtrlListView_SetItemChecked($ListViewMovingPicturesPath, -1, True)
			Case $ButtonMovingPicturesPathUncheckall
				_GUICtrlListView_SetItemChecked($ListViewMovingPicturesPath, -1, False)
			Case $ButtonMovingPicturesPathGetFolders
				MovingPicturesGetFolders()
			Case $ButtonMovingPicturesChangeDatabase
				MovingPicturesStatusUpdateNewRow("Selecting database")
				SelectDatabaseFile()
				If $sThisNewDatabaseFileHasBeenChosen = "None" Then
					; Do nothing
				Else
					GUICtrlSetData($LabelMovingPicturesDatabaseFile, "Chosen database: " & $sThisNewDatabaseFileHasBeenChosen)
					$sMovingPicturesDatabase = $sThisNewDatabaseFileHasBeenChosen
				EndIf
			Case $ButtonMovingPicturesFolderSelectionInfo
				MovingPicturesFolderSelectionInfo()
			Case $ButtonMovingPicturesCreateMetadata
				GUICtrlSetState($LabelMovingPicturesWorking, $GUI_SHOW)
				GUICtrlSetState($TabSheet2, $GUI_DISABLE)
				MovingPicturesCreateXMLandCopyPictures()
				GUICtrlSetState($LabelMovingPicturesWorking, $GUI_HIDE)
				GUICtrlSetState($TabSheet2, $GUI_ENABLE)
			Case $ButtonMovingPicturesDeleteMetadata
				GUICtrlSetState($TabSheet2, $GUI_DISABLE)
				GUICtrlSetState($LabelMovingPicturesWorking, $GUI_SHOW)
				MovingPicturesDeleteFiles()
				GUICtrlSetState($LabelMovingPicturesWorking, $GUI_HIDE)
				GUICtrlSetState($TabSheet2, $GUI_ENABLE)
			Case $RadioMovingPicturesCreateMetadata
				GUICtrlSetState($ButtonMovingPicturesDeleteMetadata, $GUI_DISABLE)
				GUICtrlSetState($ButtonMovingPicturesCreateMetadata, $GUI_ENABLE)
				GUICtrlSetData($CheckboxMovingPicturesCreateNfo, "Create .nfo file based on the information in the database")
				GUICtrlSetData($CheckboxMovingPicturesCopyFanart, "Copy Fanart picture from database location to file location")
				GUICtrlSetData($CheckboxMovingPicturesCopyCover, "Copy Cover/Poster picture from database location to file location")
				GUICtrlSetData($CheckboxMovingPicturesCopyThumb, "Copy Thumb picture from database location to file location")
				GUICtrlSetData($CheckboxMovingPicturesSameTimestampAsMedia, "Same Timestamp on new files as on media file")
				GUICtrlSetData($CheckboxMovingPicturesReplaceExistingFiles, "Replace existing files")
				;Set progress = 0
				GUICtrlSetData($ProgressMovingPictures, 0)
			Case $RadioMovingPicturesDeleteMetadata
				GUICtrlSetState($ButtonMovingPicturesCreateMetadata, $GUI_DISABLE)
				GUICtrlSetState($ButtonMovingPicturesDeleteMetadata, $GUI_ENABLE)
				GUICtrlSetData($CheckboxMovingPicturesCreateNfo, "Delete all *.nfo files in selected folders")
				GUICtrlSetData($CheckboxMovingPicturesCopyFanart, "Delete all *-fanart.jpg Fanart pictures in selected folders")
				GUICtrlSetData($CheckboxMovingPicturesCopyCover, "Delete all *-poster.jpg Cover/Poster pictures in selected folders")
				GUICtrlSetData($CheckboxMovingPicturesCopyThumb, "Delete all *-thumb.jpg Thumb pictures in selected folders")
				GUICtrlSetData($CheckboxMovingPicturesSameTimestampAsMedia, "(This is not used when deleting files)")
				GUICtrlSetData($CheckboxMovingPicturesReplaceExistingFiles, "Also delete all *.jpg *.xml *.txt. Be carefull!")
				;Set progress = 0
				GUICtrlSetData($ProgressMovingPictures, 0)
				;Stuff for TV Series tab
				;****************************
			Case $ButtonTVSeriesPathCheckAll
				_GUICtrlListView_SetItemChecked($ListViewTVSeriesPath, -1, True)
			Case $ButtonTVSeriesPathUncheckall
				_GUICtrlListView_SetItemChecked($ListViewTVSeriesPath, -1, False)
			Case $ButtonTVSeriesPathGetFolders
				TVSeriesGetFolders()
			Case $ButtonTVSeriesChangeProgramDataFolder
				TVSeriesStatusUpdateNewRow("Selecting ProgramData Folder")
				SelectProgramDataFolder()
				If $sThisNewProgramDataFolderHasBeenChosen = "None" Then
					; Do nothing
				Else
					GUICtrlSetData($LabelTVSeriesProgramDataFolder, "Chosen database: " & $sThisNewProgramDataFolderHasBeenChosen)
					$sTVSeriesDatabase = $sThisNewProgramDataFolderHasBeenChosen
				EndIf
			Case $ButtonTVSeriesChangeDatabase
				TVSeriesStatusUpdateNewRow("Selecting database")
				SelectDatabaseFile()
				If $sThisNewDatabaseFileHasBeenChosen = "None" Then
					; Do nothing
				Else
					GUICtrlSetData($LabelTVSeriesDatabaseFile, "Chosen ProgramData folder: " & $sThisNewDatabaseFileHasBeenChosen)
					$sTVSeriesDatabase = $sThisNewDatabaseFileHasBeenChosen
				EndIf
			Case $ButtonTVSeriesFolderSelectionInfo
				TVSeriesFolderSelectionInfo()
			Case $ButtonTVSeriesCreateMetadata
				GUICtrlSetState($LabelTVSeriesWorking, $GUI_SHOW)
				GUICtrlSetState($TabSheet1, $GUI_DISABLE)
				TVSeriesCreateXMLandCopyPictures()
				GUICtrlSetState($LabelTVSeriesWorking, $GUI_HIDE)
				GUICtrlSetState($TabSheet1, $GUI_ENABLE)
			Case $RadioTVSeriesCreateMetadata
				GUICtrlSetState($ButtonTVSeriesDeleteMetadata, $GUI_DISABLE)
				GUICtrlSetState($ButtonTVSeriesCreateMetadata, $GUI_ENABLE)
				GUICtrlSetData($CheckboxTVSeriesCreateSeriesNfo, "Create SERIES .nfo file based on the information in the database")
				GUICtrlSetData($CheckboxTVSeriesCopySeriesFanart, "Copy SERIES Fanart pictures")
				GUICtrlSetData($CheckboxTVSeriesCopySeriesSeasonPoster, "Copy SERIES Season Poster pics")
				GUICtrlSetData($CheckboxTVSeriesCopySeriesBanner, "Copy SERIES Banner picture")
				GUICtrlSetData($CheckboxTVSeriesCopySeriesPoster, "Copy SERIES Poster picture")
				GUICtrlSetData($CheckboxTVSeriesCreateEpisodesNfo, "Create EPISODES .nfo files based on the information in the database")
				GUICtrlSetData($CheckboxTVSeriesCopyEpisodePics, "Copy EPISODE pictures")
				GUICtrlSetData($CheckboxTVSeriesSameTimestampAsMedia, "Same Timestamp on new EPISODE files as on media file")
				GUICtrlSetData($CheckboxTVSeriesReplaceExistingFiles, "Replace existing files")
				GUICtrlSetData($ProgressTVSeries, 0)
			Case $RadioTVSeriesDeleteMetadata
				GUICtrlSetState($ButtonTVSeriesDeleteMetadata, $GUI_ENABLE)
				GUICtrlSetState($ButtonTVSeriesCreateMetadata, $GUI_DISABLE)
				GUICtrlSetData($CheckboxTVSeriesCreateSeriesNfo, "Delete all SERIES *.nfo files in selected folders")
				GUICtrlSetData($CheckboxTVSeriesCopySeriesFanart, "Delete all SERIES Fanart pictures")
				GUICtrlSetData($CheckboxTVSeriesCopySeriesSeasonPoster, "Delete SERIES Season Poster pics")
				GUICtrlSetData($CheckboxTVSeriesCopySeriesBanner, "Delete all SERIES Banner pictures")
				GUICtrlSetData($CheckboxTVSeriesCopySeriesPoster, "Delete all SERIES Poster pictures")
				GUICtrlSetData($CheckboxTVSeriesCreateEpisodesNfo, "Delete ALL *.nfo files in selected folders. Be careful!")
				GUICtrlSetData($CheckboxTVSeriesCopyEpisodePics, "Delete ALL *.jpg files in selected folders. Be careful!")
				GUICtrlSetData($CheckboxTVSeriesSameTimestampAsMedia, "(This is not used when deleting files)")
				GUICtrlSetData($CheckboxTVSeriesReplaceExistingFiles, "(This is not used when deleting files)")
				GUICtrlSetData($ProgressTVSeries, 0)
			Case $ButtonTVSeriesDeleteMetadata
				TVSeriesDeleteFiles()
		EndSwitch
	WEnd
EndFunc   ;==>StartMainGUI







